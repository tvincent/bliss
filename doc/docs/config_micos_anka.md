# Micos Motor controller

Below an example of the yaml configuration file

## Configuration example
```YAML
   - controller:
  module: motors.micos_anka
  class: MicosAnka
  name: Sam
  steps: 26222
  tcp:
    url: lamino.esrf.fr
  axes:
  - name: sx45
    address: 1
    velocity: 10.0
    steps_per_unit: 1
    acceleration: 20.0
    unit: mm
    tolerance: 0.001

  - name: sy45
    address: 2
    velocity: 10.0
    steps_per_unit: 1
    acceleration: 20.0
    unit: mm
    tolerance: 0.001
```
