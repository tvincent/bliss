

# Convention for kinematics calculations:

We are using a direct coordiante system (x,y,z) where the origin is the center
of rotation of the system.

```
                ^
                | Z axis
                |
    X axis      |
      <----------
               /
              /
             / Y axis
            /
            v
```

The calculated axes are named **rx**, **ry** around respectively the X and Y
axis with counter-clockwize positive value and **tz** vertical translation.

All parameters relative to legs are following the same convention and are given
in meters:

    - coordinates of "lega" are named: "ax", "ay"
    - coordinates of "legb" are named: "bx", "by"
    - coordinates of "legc" are named: "cx", "cy"

The calculation takes into account the `unit` defined for each real or
calculated motors. If the `unit` field is missing, the universal units are
used: meters (m) for distance, radians (rad) for angles.


The rotation center of the system can be changed using:
```
fjsz.controller.center = <center_id>
```

# Configuration Example

Configuration with two centers of rotation:

```yaml
- plugin: emotion
  module: tripod
  class: tripod
  name: fjs_tripod
  centers:
    ### Si111
    - center_id: hall
      ax: -0.140
      ay: -0.1525
      bx: -0.140
      by: 0.0675
      cx: 0.140
      cy: -0.0425
    ### Si311
    - center_id: ring
      ax: -0.140
      ay: -0.0675
      bx: -0.140
      by: 0.1525
      cx: 0.140
      cy: 0.0425
  axes:
    - name: $mfjsur
      tags: real lega
    - name: $mfjsuh
      tags: real legb
    - name: $mfjsd
      tags: real legc

    - name: fjsz
      unit: mm
      tags: tz
    - name: fjsrx
      unit: mrad
      tags: rx
    - name: fjsry
      unit: mrad
      tags: ry
```
