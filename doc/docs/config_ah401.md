# AH401 controller

## Description

Controller class for the AH401 models B and D from CAEN (www.caenels.com). 

AH401 is a 4-channel, 20-bit resolution, low noise, high performance charge-integration picoammeter.

It is composed of a particular charge-integration input stage for low current sensing, 
coupled with a 20-bit sigma-delta ADC converter including a noise reduction digital filter.
Model D is able to use different integration ranges for channels 1-2 and channels 3-4, 
whereas model B uses same range for all channels (see self.scale_range).

The AH401D performs current measurements from 50 pA (with a resolution of 50 aA) up to 2.0 μA (with a resolution of 2.0 pA), 
with integration times ranging from 1ms up to 1s. Moreover, each input channel has two parallel integrator stages, 
so that the current-to-voltage conversion can be performed continuously also during the ADC conversion, 
avoiding any dead time in the data throughput (see self.half_mode).

It also performs digitization of the acquired current data, thus strongly minimizing the transmission length of 
analog current signals from the detector and providing directly digital data output, which is easy to transmit 
over long distances without any noise pick-up.

The AH401D is housed in a light, practical and extremely compact metallic box that can be placed as close as 
possible to the current source (detector) in order to reduce cable lengths and minimize possible noise pick-up 
during the propagation of very low intensity analog signals. It is specially suited for applications where multi-channel 
simultaneous acquisitions are required, a typical application being the currents readout from 4-quadrant photodiodes 
used to monitor X-ray beam displacements. Low temperature drifts, good linearity and the very low intrinsic noise of 
the AH401D allow obtaining very high precision current measurements.

The AH401D uses a standard Ethernet TCP communication layer with the DEFAULT_PORT = 10001.


## Configuration

### YAML configuration file example


```YAML
  - name: ah401 
    class: Ah401
    plugin: generic
    module: ah401
    
    tcp:
        #url: 'bm32pico2:10001'
        url: '160.103.123.129:10001'

    counting_mode: STD

    counters:
      - name: pico_ch1
        channel: 1
        mode: MEAN
        unit: nA
    
      - name: pico_ch2
        channel: 2
        mode: MEAN
        unit: nA

      - name: pico_ch3
        channel: 3
        mode: MEAN
        unit: nA

      - name: pico_ch4
        channel: 4
        mode: MEAN
        unit: nA
```



### Special modes and options


**half_mode**:

The purpose of this mode is to select whether to process data from both integrator circuits (i.e. maximum speed, half_mode disabled) 
or only from one integrator circuit (i.e. best noise performance, half_mode enabled) of the AH401D.

- If `half_mode` is disabled (=False):

    The AH401D performs the current-to-voltage integration, continuously in time, using both channels (A and B) in parallel.
    when one of the integrator A is digitized by the ADC, the input channel is switched to the other integrator circuit (i.e. B) 
    and the input current can thus be continuously integrated. 
    At the end of the integration time, also the second integrator (i.e. B) is disconnected from the input and the ADC can start the 
    digitization of the integrated current from B. At the same time, the input is connected to the previous integrator (i.e. A) 
    and therefore the current can continue to be integrated by A.
    This sequence is continuously repeated as long as the AH401D is acquiring. 
    Dead time in the data throughput is thus avoided and the sampled values are continuously sent to the host. 
    This mode of operation (default) is useful when the maximum sampling rate is required since at the end of each 
    integration cycle a digitized data set is sent to the host PC. 
    The drawback is a slightly higher noise level on the sampled values due to the integrator capacitors mismatch between A and B 
    and to the charge injection introduced by the internal switches.

- If `half_mode` is enabled (=True):

    If lowest noise performance is of uttermost importance, the half mode mode must always be enabled. 
    In this operation mode only the integrated current of one integrator (i.e. A) is sampled, digitized and sent to the host. 
    Using only and always the same integrator any capacitors mismatch and charge injection is avoided and the best signal to noise ratio is achieved. 
    The operating sequence of this mode is as follow: the current is integrated on integrator A for the integration time (i.e. 100ms), 
    then the value is digitized and sent to the host. 
    During the following integration time (i.e. 100ms) no data is digitized (the value on the integrator B is discarded) and then the sequence repeats itself. 
    Therefore a set of data is sent to the host every two integration times (i.e. 200ms). 
    The drawback of this mode is that only half sampled values are sent to the host and hence the sampling rate is halved.

Please note that the data rate throughput is directly related to the integration time and the `half_mode` selection. 
For example, setting the integration time to 10ms and the `half_mode` to False (disabled) generates a data stream at 100Hz. 
Whereas, setting the integration time to 10ms and the `half_mode` to True (enabled), generates a data stream at 50Hz.

**sample_number**:

The purpose of the sample_number is to define a fixed number of samples to acquire after starting an acquisition with 
the `acquistion_start` command. Once the samples are acquired the acquisition stops automatically.
The sample number should be in range [1, 2e7] or set to 0 to disable this behavior and allow continous data acquisition.
If a number of acquisitions larger than 4096 is set, the `sum_mode` is automatically disabled.


**scale_range**:

If value has 2 digits, the first digit is the range applied to channels 1-2,
and second digit the range applied to channel 3-4.
If value has a single digit, it is applied to all channels.


**sum_mode**:

The purpose of this mode is to add the values of `N` data samples configured with the `sample_number` command 
and hence to get a single channel value representing the summed samples.
In order to avoid data overflow, the sum mode cannot be enabled if the number of acquisitions 
set via the `sample_number` command is larger than 4096.


**trigger_mode**:

- `SOFTWARE`:

    When staring an acquistion with the `acquistion_start` command, the Ah401 starts to acquire data continuously.

      * If `sample_number` == 0:

          The Ah401 acquires data continuously until acquisition is stopped with the `acquisition_stop` command.

      * If `sample_number` != 0:

          If the `sample_number` is not zero, the acquisition automatically stops after the configured number of samples is acquired.
          Moreover, if the `sum_mode` is enabled, only the summed values of the samples are returned.

- `HARDWARE`:

    When staring an acquistion with the `acquistion_start` command, the Ah401 waits to receive a falling edge signal. 
    As soon as this signal is detected, the AH401 starts to acquire data continuously.

    * If `sample_number` == 0:

        When a second signal is received, the acquisition is paused. Then, another signal will resume the acquisition (unpause).
        This behavior is repeated until acquisition is stopped with the `acquisition_stop` command. 

    * If `sample_number` != 0:
    
        If the `sample_number` is not zero, the acquisition automatically stops after the configured number of samples is acquired 
        and the instrument waits for a new TRIGGER signal. 
        Moreover, if the `sum_mode` is enabled, only the summed values of the samples are returned.
        This behaviour continues until acquisition is stopped with the `acquisition_stop` command.

**counting_mode**:

If this mode is set to `STD` the integration_time of the ah401 is decoupled from the count_time value passed to a scan command.
If this mode is set to `AUTO`, the count_time value of a scan command is applied as the ah401 integration time (if in the range [1ms, 1s]).