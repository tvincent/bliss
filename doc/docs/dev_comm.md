
# Communication protocols

BLISS implements the following communication protocols:

- [Serial line](dev_comm.md#serial-line)
- [Ser2net](dev_comm.md#ser2net)
- [TCP socket](dev_comm.md#tcp-socket)
- [UDP socket](dev_comm.md#udp-socket)
- [GPIB](dev_comm.md#gpib)
- [Modbus](dev_comm.md#modbus)
- [VXI11](dev_comm.md#vxi11)
- [SCPI](dev_comm.md#scpi)

## Helper function `get_comm` 

To create the communication object, BLISS provides a unique helper function:

```python
def get_comm(config, ctype=None, **opts)
```

This helper creates the communication object from the YAML configuration or a simple dictionary (`config`).
If a communication option is not found in `config`, it will look in the default options dictionary `opts`.
If the option cannot be found in either `config` or `opts`, the communication class will use its own default.
The type of communication deduced from the configuration can by checked by providing the `ctype` argument.

!!!example "Example with serial line"

    ```python
    from bliss.comm.util import get_comm, SERIAL
    config = { 'serial': {'url': '/dev/ttyS0'}, }
    default_options = {'baudrate': 19200, 'eol': b'\r'}
    comm = get_comm(config, ctype=SERIAL, **default_options)
    ```

### From YAML configuration file

!!!example "YAML example"

    ```yaml
    name: ser0
    plugin: comm
    serial:
      url: /dev/ttyS0
      eol: "\\r\\n"
    ```

```python
from bliss.config.static import get_config
ser0 = get_config().get('ser0')
```

!!!note
    Notice that it requires the `comm` plugin.

### Within a controller

!!!example "YAML example"

    ```yaml
    class: FooController
    name: foo
    serial:
      url: /dev/ttyS0
    ```
    
```python
from bliss.comm.util import get_comm, SERIAL

class FooController:
    def __init__(self, config):
        self.config = config
        self._comm = None

    def _init_com(self):
        default_options = {'baudrate': 19200}
        self._comm = get_comm(self.config, ctype=SERIAL, **default_options)
```




## Serial line

https://en.wikipedia.org/wiki/Serial_port

### YML configuration

```yaml
serial:
  url: /dev/ttyS0
```

### Mandatory parameters

* `url`: serial device file name

### Optional parameters

* `baudrate`:
    * Values: [`1200`, `2400`, `4800`, `9600`, `19200`, `38400`, `57600`, `115200`]
    * Default: `9600`

* `bytesize`:
    * Values: [`7` (true ASCII) , `8` (most cases)]
    * Default: `8`

* `dsrdtr`: enable hardware flow control (DSR/DTR)
    * Values: [`False`, `True`]
    * Default: `False`

* `eol`: end-of-line string to READ data
    * Default: `b"\n"`

* `interCharTimeout`: inter byte timeout setting
    * Default: `None`
    * NB: `inter_byte_timeout` in new PySerial version

* `parity`:
    * Values: [`'N'`: none, `'O'`: odd, `'E'`: even, `'M'`: mark, `'S'`: space]
    * Default: `'N'`

* `port`:
    * Default: same as `url`

* `rtscts`: enable hardware flow control (RTS/CTS)
    * Values: [`False`, `True`]
    * Default: `False`

* `stopbits`:
    * Default: `1`

* `timeout`:
    * Default: `5.0`

* `writeTimeout`:
    * Default: `None`

* `xonxoff`:
    * Values: [`False`, `True`]
    * Default: `False`




### Serial BLISS devices structure
<!--
ACHTUNG: original file is  img/comm/serial_hierarchy.svg
it must be transformed in PATH only file in order to avoid fonts problem
with : shift-Ctrl-C and saved as img/comm/serial_hierarchy_paths.svg
-->
![Screenshot](img/comm/serial_hierarchy.png)




## Ser2net

Ser2net (aka rfc2217) is a protocol to deport serial line over ethernet.

Such a remote serial line can be used in `rfc2217` mode or `ser2net`
mode.

`ser2net` mode allows to define the remote serial device to use in local
config (considering a well configured ser2net server with control port)

`rfc2217` mode uses the mapping *"port <-> serial device"* defined on the
 remote host in ser2net config file

### YML configuration

```yaml
serial:
  url: ser2net://lidXXX:29000/dev/ttyRP11
```

or:

```yaml
serial:
  url: rfc2217://lidXXX:28001
```

### Mandatory parameters

[See serial line parameters](dev_comm.md#mandatory-parameters)
### Optional parameters

[See serial line parameters](dev_comm.md#optional-parameters)


## TCP socket

### YML configuration
```yaml
tcp:
  url: 160.103.99.42
  eol: "\\r\\n" # Mind the double quotes and escaping the backslash
```

### Mandatory parameters

* `url`: IP address or a fully qualified name
    * examples:
        * `160.103.14.92`
        * `zorglub.esrf.fr`

### Optional parameters

* `port`: target host's port
    * example: `5025`

* `timeout`: 
    * Default: 5.0

* `eol`: end-of-line string to READ data
    * Default: `b"\n"`

### Ethernet BLISS devices structure
<!--
ACHTUNG: original file is  img/comm/ethernet_class_hierarchy.svg
it must be transformed in PATH only file in order to avoid fonts problem
with : shift-Ctrl-C and saved as img/comm/ethernet_class_hierarchy_paths.svg
-->
![Screenshot](img/comm/ethernet_class_hierarchy.png)


## GPIB

There are various ways to communicate with GPIB devices:

* GPIB PCI board
    - locally if BLISS runs on the same computer than the GPIB driver
    - remotely if BLISS runs on another computer
* GPIB ethernet device: Enet box

Example of GPIB communication with PCI GPIB board and tango device server:
```yaml
gpib:
  url: tango_gpib_device_server://id42/gpib_lid421/0
  pad: 13
  timeout: 10.
```

Example of GPIB communication with Enet device:
```yaml
gpib:
  url: enet://gpibid42a.esrf.fr
  pad: 15
  timeout: 3.
```

## UDP socket

TODO

## SCPI

TODO

## Modbus

TODO

## VXI11

TODO

