# DEltaElektronicaPscEth configuration

Controller for DeltaElektronica PscEth Power Supply.

The controller class provides basic access to:

- voltage (Volt) measured value, setpoint and maximum value
- current (Amp) measured value, setpoint and maximum value
- power (W) computed on voltage and current measured
- enable/disable output power
- enable/disable voltage and/or current remote control

on()/off() method to enable/disable in one-go:

- output power
- voltage remote control
- current remote control

Optionnaly, counters can be configured for measured value of:

- voltage
- current
- power

## Configuration example (yml)

```yaml
- class: DeltaElektronicaPscEth
  plugin: generic
  module: powersupply.DeltaElektronicaPscEth
  name: deltaps
  tcp:
    url: deltabcu1.esrf.fr:8462
  counters:
    - name : de_volt
      tag: voltage
    - name: de_curr
      tag: current
    - name: de_power
      tag: power
```

