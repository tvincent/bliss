# Newport SMC100 motor controller

Bliss controller for Newport SMC100 motor controller.

The controller communicates via serial line. Baudrate (57600) and terminator (CR/LF) are configured within the controller. Other parameters are default ones (data bits=8, parity=None).

!!!note
    Only SMC100_CC controller has been tested. SMC100_PP also exist for stepper motors.

On controller restart, the axis is in NOTREFENCED state. A homing has to be performed before using it.

## Supported features

Encoder | Shutter | Trajectories
------- | ------- | ------------
No      | No      | No


## Configuration

!!!note
    By default, controller has address 1. Address is changed only if using multiple controller in a daisy chain.


Example:
```YAML
controller:
  class: NewportSMC100
  module: newport
  serial:
    url: tango://id00/serial_001/1
  axes:
    - name: smc1
      steps_per_unit: 1
      velocity: 25.0
      acceleration: 100.0
      tolerance: 0.001
      address: 1
```

## Specific methods

Get a detailed status from controller:
```
NEWPORT [6]: smc1.read_status()
CONTROLLER STATE : READY from DISABLE
CONTROLLER ERRORS : NONE.
BLISS AXIS STATE :
  READY (Axis is READY)
```

Read stage identification:
```
NEWPORT [7]: smc1.read_stage()
STAGE : VP-25XA_PN:B147328_UD:142410
```

Read hardware configured limits:
```
NEWPORT [8]:
NEWPORT [8]: smc1.read_limits()
    Out [8]: ('-12.5', '12.5')
```
These limits can be set to bliss software limits using `smc1.configure_limits()`.

Read hardware specific parameters:

```
NEWPORT [10]: smc1.read_parameters()
    Out [10]: {'low_pass_filter': '1000', 'following_error': '0.1', 'friction_compensation': '0', 'jerk_time': '0.04', 'derivative_gain': '1.99951', 'integral_gain': '12487.8', 'proportionnal_gain': '498.047', 'velocity_feed_forward': '0.541992'}
```

To change this parameters, use:
```
NEWPORT [11]: c = smc1.read_parameters()
NEWPORT [12]: c["integral_gain"] = 12487.8+1
NEWPORT [13]: smc1.write_parameters(c)
Go to DISABLED state
Set integral_gain = 12488.8
Go to READY state
NEWPORT [14]:
```

The method `smc1.configure_stage()` can be used to upload configuration from an ESP stage. This should not be needed as an ESP upload is performed on controller reset.

To Reset controller, use `smc1.reset_controller()`.

