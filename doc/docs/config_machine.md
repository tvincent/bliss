

BLISS offers some mechanisms to deal with information obtained from the ESRF
accelerator.

`MachInfo` class provides:

* counters to access to accelerator information
    - `sr_mode`:  operation mode
    - `automatic_mode`: automatic FE mode activation
    - `tango_uri`: address of the Front-End Tango device server
* counters to access to accelerator information:
    - `current`
    - `lifetime`
    - `sbcurr`
    - `refill`
* helper functions to implement refill pretection in a sequence
    - `iter_wait_for_refill()`
    - `check_for_refill()`
    - `WaitForRefillPreset`


## Configuration

Configuration example:
```yaml
- class: MachInfo
  plugin: bliss
  uri: //acs.esrf.fr:10000/fe/master/id42
  name: machinfo
```


## machinfo properties

* `sr_mode`: operation mode in: [`USM`; `MDT`; `Shutdown`; `SafetyTest`; `IdTest`]
* `automatic_mode`: activation of automatic FE mode: `True` or `False`
* `tango_uri`: address of the Front-End Tango device server
* `counters`:
    - `current`: Ring current of the machine
    - `lifetime`: Remaining Lifetime of the beam
    - `sbcurr`: Ring current of the machine in Single Bunch mode
    - `refill`: Countdown to the Refill


Counters usage in a scan:
```pyton

DEMO [7]: ct(1, machinfo)
   Thu Mar 09 11:28:10 2023: Scan(name=ct, path='not saved')
            current  =       68.9400      mA (      68.9400     mA/s)  machinfo
           lifetime  =    18986.1            (   18986.1          /s)  machinfo
             sbcurr  =        4.48000     mA (       4.48000    mA/s)  machinfo
             refill  =     1910.00       sec (    1910.00      sec/s)  machinfo

   Took 0:00:01.014416[s]

  Out [7]: Scan(name=ct, path='not saved')
```

Reading of a particular counter:
```python
DEMO [9]: machinfo.counters.current
 Out [9]: 'current` Tango attribute counter info:
            device server = //acs.esrf.fr:10000/fe/master/id42
            Tango attribute = SR_Current
            Tango format = "%6.2f"
            Tango unit = "mA"
            scalar
            value: 8.82

DEMO [10]: machinfo.counters.current.value
 Out [10]: 194.43
```


### `__info__`

`MachInfo` object shell info provides:

* url of the tango device used
* AutoMode timing
* Operator Message

```pyton
DEMO  [5]: machinfo
  Out [5]: MACHINE INFORMATION   ( //acs.esrf.fr:10000/fe/master/id42 )

           -----------------  ---------------------------------------------
           SR Mode:           USM
           Current:           8.79 mA
           Lifetime:          245231s = 2days 20h 7mn 11s
           Refill CountDown:  6519s = 1h 48mn 39s
           Filling Mode:      7/8 multibunch
           AutoMode:          True (remaining: 498183s = 5days 18h 23mn 3s)
           -----------------  ---------------------------------------------
           Operator Message: Jun  3 12:00 Back to USM
```



### all_information

`all_information` property returns most of all the machine information as a
dictionary:

```python
DEMO [9]: pprint.pprint(machinfo.all_information)
{'Auto_Mode_Time': 502822,
 'Automatic_Mode': True,
 'EXP_Itlk_State': tango._tango.DevState.ON,
 'FE_Itlk_State': tango._tango.DevState.ON,
 'FE_State': 'FE open',
 'HQPS_Itlk_State': tango._tango.DevState.ON,
 'PSS_Itlk_State': tango._tango.DevState.ON,
 'SR_Current': 195.93514894706684,
 'SR_Filling_Mode': '7/8 multibunch',
 'SR_Lifetime': 66281.93637072828,
 'SR_Mode': 'USM',
 'SR_Operator_Mesg': 'Jun  3 12:00 Back to USM',
 'SR_Refill_Countdown': 2034.0,
 'SR_Single_Bunch_Current': -0.04008001020600001,
 'UHV_Valve_State': tango._tango.DevState.ON}
```

## Refill Checks


### WaitForRefillPreset

The simplest way to check for refills is to activate the preset named
`WaitForRefillPreset` by setting `check` property to `True`.
This will add the preset to the presets of the default chain.

```python

DEMO [37]: machinfo.check
 Out [37]: False

DEMO [38]: DEFAULT_CHAIN._presets
 Out [38]: {}

DEMO [39]: machinfo.check=True
Activating Wait For Refill on scans

DEMO [40]: DEFAULT_CHAIN._presets
 Out [40]: {'MACHINE': <bliss.controllers.machinfo.WaitForRefillPreset
                        object at 0x7f31d188d650>}

```

This preset will pause a scan:

* during the refill
* or if the `checktime` is greater than the **time to refill**.

If `checktime` is set to `None`, `count_time` is used if found on the top
master of the chain.

Insertion of `WaitForRefillPreset` preset is done via `.check` setting:

* Set it to `True` to activate refill check.
* Set it to `False` to de-activate refill check.

Example:
```python

DEMO [6]: machinfo.check
 Out [6]: False

DEMO [7]: machinfo.check=True

```


### iter_wait_for_refill

`iter_wait_for_refill(<checktime>, <waittime>=0., <polling_time>=1.)`

Helper for waiting the machine refill. It yields two states:
`WAIT_INJECTION` and `WAITING_AFTER_BEAM_IS_BACK` until the machine refill is
finished.

Nothing is returned if the beam is ok.

Example:
```python
for status in iter_wait_for_refill(my_check_time,waittime=1.,polling_time=1.):
    if status == "WAIT_INJECTION":
        print("Scan is paused, waiting injection",end='\r')
    else:
        print("Scan will restart in 1 second...",end='\r')
```

### check_for_refill

`check_for_refill(checktime)`: Return True if `<checktime>` (in seconds) is
*smaller* than the refill countdown.
ie: return True if a task of length `<checktime>` can be performed before the
refill.

Example:
```python
DEMO [4]: from bliss.controllers.machinfo import  MachInfo

DEMO [5]: machinfo.counters.refill
 Out [5]: 'refill` Tango attribute counter info:
             device server = //acs.esrf.fr:10000/fe/master/id42
             Tango attribute = SR_Refill_Countdown
             ...
             value: 6411.0

DEMO [6]: MachInfo.check_for_refill(machinfo, 6300)
 Out [6]: True

DEMO [7]: MachInfo.check_for_refill(machinfo, 6500)
 Out [7]: False
```
