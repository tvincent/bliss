## Configuring an PI Hexapod motor controller

This chapter explains how to configure an PI Hexapod motor controller.

### Supported features

Encoder | Shutter | Trajectories 
------- | ------- | ------------ 
NO	| NO      | NO          

### Specific PI Hexapod controller parameters

* **model**: controller model either 850 or 887 (optional)

Model 850 is communicating via serial line and model 887 via tcp sockets.

### Specific PI Hexapod axis parameters

* **channel**: X,Y,Z,U,V or W

### YAML configuration file example

For 850 model:
```YAML
class: PI_HEXA
model: 850
serial:
  url: ser2net://lid133:28000/dev/ttyR37
```
For 887 model:
```YAML
class: PI_HEXA
model: 887
tcp:
    url: pihexaid031
```
Followed by axes configuration:
```YAML
axes:
- name: nnx
  channel: X
  low_limit: -0.2
  high_limit: 0.2
  steps_per_unit: 1
  tolerance: 0.001
- name: nny
  channel: Y
  low_limit: -3.0
  high_limit: 3.0
  steps_per_unit: 1
  tolerance: 0.001
- name: nnz
  channel: Z
  low_limit: -8.0
  high_limit: 8.0
  steps_per_unit: 1
  tolerance: 0.001
- name: nnu
  channel: U
  low_limit: -1.0
  high_limit: 1.0
  steps_per_unit: 1
  tolerance: 0.001
- name: nnv
  channel: V
  low_limit: -1.0
  high_limit: 1.0
  steps_per_unit: 1
  tolerance: 0.001
- name: nnw
  channel: W
  low_limit: -2.0
  high_limit: 2.0
  steps_per_unit: 1
  tolerance: 0.001
```

### Specific PI Hexapod methods on axis

- `get_hw_limits()` : returns hardware set limits. Can help to setup software limits.
- `get_rotation_origin()/set_rotation_origin()` : change origin point for rotations
