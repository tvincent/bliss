# MCCE Device Server #

## Launching the device server ##

### From command line (mainly for testing) ###

```
(base) user@beamline:~/bliss$ . blissenv
(bliss) user@beamline:~/bliss$ export TANGO_HOST=my_tango_host:20000
(bliss) user@beamline:~/bliss$ MCCE -?
usage :  MCCE instance_name [-v[trace level]] [-nodb [-dlist <device name list>]]
Instance name defined in database for server MCCE :
        mcce_1_1
(bliss) user@beamline:~/bliss$ MCCE mcce_1_1
Ready to accept request
```

### With supervisor ###
Create a *.conf file:

```
[group: MCCE]
programs=mcce1_1

[program:mcce1_1]
command=bash -c "source /users/blissadm/conda/miniconda/bin/activate bliss &
& exec python /users/blissadm/conda/miniconda/envs/bliss/bin/MCCE mcce_1_1"
environment=TANGO_HOST="my_tango_host:20000",HOME="/users/blissadm"
user=opid12
startsecs=2
autostart=true
redirect_stderr=true
stdout_logfile=/var/log/%(program_name)s.log
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
stdout_capture_maxbytes=1MB
```

## Configuration ##

### With Beacon ###

The configuration of this device server is written inside Beacon as
an yaml file, here we have the example:

```yaml
device:
- tango_name: id00/mcce/1_1
  class: MCCE
  properties:
    beacon_name: mcce1_1
personal_name: mcce_1_1
server: MCCE
```

- tango_name: is the Tango Device in the form *domain/family/member*
- personal_name: this will be the name you will use in the command line to
launch the Device Server using **MCCE personal_name**
- beacon_name: should corespond to another Beacon object defined in yml that
will define MCCE bliss controller configuration


