# Meestetter LRT1200

Accessible via Ethernet using TEC-Family multi channels controllers: 

- TEC-1089
- TEC-1090
- TEC1091
- TEC-1122
- TEC-1123

### YAML two channels configuration example :

```YAML
- class: Ltr1200
  module: regulation.temperature.meerstetter.ltr1200
  plugin: regulation
  #host: id10mer1
  host: 192.168.0.1

  outputs:
    - name: ltr1200output1
      unit: A
      channel: 1
    - name: ltr1200output2
      unit: A
      channel: 2

  inputs:
    - name: ltr1200input1
      unit: Celsius
      channel: 1
    - name: ltr1200inputsafety1
      unit: Celsius
      channel: 1
      type: Sink
    - name: ltr1200input2
      unit: Celsius
      channel: 2
    - name: ltr1200inputsafety2
      unit: Celsius
      channel: 2
      type: Sink         

  ctrl_loops:
    - name: ltr1200loop1
      channel: 1
      input: $ltr1200input1
      output: $ltr1200output1
    - name: ltr1200loop2
      channel: 2    
      input: $ltr1200input2
      output: $ltr1200output2
```
