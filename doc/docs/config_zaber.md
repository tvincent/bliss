## Configuring a Zaber motion controller

This section explains how to configure a Zaber motion controller.
It has beed tested on a X-MCB2 type controller with one peripheral connected (LRQ150DP-EO1T3 linear stage)

check manufacturer's documentation:

 - https://www.zaber.com/software/docs/motion-library/ascii 
 - https://www.zaber.com/manuals/X-MCB2


###  Supported features

Encoder | Shutter | Trajectories
------- | ------- | ------------
NO	| NO      | NO  

### Example YAML configuration:

```yaml
-
 controller:
     plugin: emotion
     class: Zaber
     name: zaber
     serial:
         url: "rfc2217://lid269:28410"
         baudrate: 115200
     axes:
      - name: zamot
        channel: 1
        velocity: 10 
        acceleration: 5
        steps_per_unit: 1000000 
        high_limit: 150
        low_limit: 0
        velocity_high_limit: 819200
        velocity_low_limit: 1
```
