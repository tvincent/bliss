# Live image

For scans containing image data, the image widget will be displayed automatically.

A specific widget will be created per detector.

Some light data processing can be enabled to improve the display.

![Flint screenshot](../img/flint-image-widget.png)

Only the last retrieved data from the detector will be displayed. There is no
way for now to browse the data from a scan.

To follow the evolution of an indicator in the time, it is possible to create
first ROIs (see [editing Lima ROIs](live_image_lima_roi.md)), and then acquiring
a scan on it. THe data will be then displayed in the default curve plot.

## APIs

Here is the available commands which can be used from the BLISS shell.

### Get the plot

The plot is reachable using the BLISS name of the detector.

```python
f = flint()
ct(tomocam)
f.wait_end_of_scans()

p = f.get_live_plot(image_detector="tomocam")
```

### Interactive data selection

This plot provides an API to [interact with region of interest](../flint_interaction.md).

### Colormap

An API is provided to custom the colormap.

```python
ct(tomocam)

# Make sure the scan was also completed in flint side
f = flint()
ct(tomocam)
f.wait_end_of_scans()

p = f.get_live_plot(image_detector="tomocam")

p.set_colormap(lut="gray",
               vmin=0, vmax="auto",
               normalization="log",
               autoscale_mode="stddev3")
```

### Logbook

```
p.export_to_logbook()
```

### Markers

A marker API is provider to create/update/remove permanent markers.

This markers are controller by BLISS and are persistent.
Which is not the case for the interaction API also provided.

If this markers are `editable` (which is the default), the user can move them
manually with the mouse.

The user can also remove them using the GUI (this could be protected in the
future, if requested by beamlines).

```python
# Get a live plot
f = flint()
f.wait_end_of_scans()
ct(tomocam)
p = f.get_live_plot(image_detector="tomocam")

# Create a marker
p.update_marker("my-marker-id", (10, 11), editable=False, text="Hi mom!")

# Move a marker
p.update_marker("my-marker-id", (20, 21))

# Print the actual location of the marker
pos = p.marker_position("my-marker-id")
print(f"x-pos {pos[0]} and y-pos {pos[1]}")

# Remove a marker
p.remove_marker("my-marker-id")
```

### Lima ROI edition

A live image widget is usually linked to a 2D Lima detector.

This widget will became the place to edit the ROIs of this detector if it
triggered.

See [editing Lima ROIs](live_image_lima_roi.md)

### Data processing

The image viewer provides few filters to normalize the image data in Flint side.

Some of the API can be reachable from a `processing` property.

```
# Get a live plot
p = f.get_live_plot(image_detector="tomocam")

# Create a dummy data
import numpy
data = numpy.zeros((1024, 1024))

# Update the mask used to exclud pixels from the contrast computation
p.processing.set_mask(data)

# Update the filter used for flatfield correction
p.processing.set_dark(data, expotime=1.0)
p.processing.set_flat(data, expotime=2.0)
```
