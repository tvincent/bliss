[![build status](https://gitlab.esrf.fr/bliss/bliss/badges/master/pipeline.svg)](https://gitlab.esrf.fr/bliss/bliss/pipelines/master/latest)
<!-- [![coverage report](https://gitlab.esrf.fr/bliss/bliss/badges/master/coverage.svg)](https://bliss.gitlab-pages.esrf.fr/bliss/master/htmlcov) -->


# Documentation

- [latest](https://bliss.gitlab-pages.esrf.fr/bliss/master)
- [1.11.x](https://bliss.gitlab-pages.esrf.fr/bliss/1.11.x)
- [1.10.x](https://bliss.gitlab-pages.esrf.fr/bliss/1.10.x)

# How to install

Installation at an ESRF beamline: **https://bliss.gitlab-pages.esrf.fr/bliss/master/installation_esrf.html**

Installation anywhere else: **https://bliss.gitlab-pages.esrf.fr/bliss/master/installation.html**
