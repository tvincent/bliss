import asyncio  # noqa: E402
import time  # noqa: E402
import gevent  # noqa: E402

from bliss.common.greenlet_utils import asyncio_gevent

# test_asyncio_on_gevent_will_run_in_dummy_thread


def current_greenlet_is_main():
    # cannot use gevent.getcurrent() is gevent.get_hub() because
    # gevent.getcurrent() can return gevent.Greenlet or greenlet.greenlet objects
    return gevent.getcurrent().parent is None


def test_asyncio_on_gevent_will_run_in_dummy_thread_with_asyncio_run():
    assert current_greenlet_is_main()

    result = asyncio.run(asyncio_on_gevent_will_run_in_dummy_thread_async(None))
    assert result == 42


def test_asyncio_on_gevent_will_run_in_dummy_thread_with_future_to_greenlet():
    assert current_greenlet_is_main()
    greenlet = asyncio_gevent.future_to_greenlet(
        asyncio_on_gevent_will_run_in_dummy_thread_async(gevent.get_hub()),
    )
    result = greenlet.get()
    assert result == 42


async def asyncio_on_gevent_will_run_in_dummy_thread_async(parent):
    assert gevent.getcurrent().parent is parent
    return 42


# test_asyncio_on_gevent_supports_nested_async_calls


def test_asyncio_on_gevent_supports_nested_async_calls_with_asyncio_run():
    result = asyncio.run(asyncio_on_gevent_supports_nested_async_calls_1())
    assert result == 42


def test_asyncio_on_gevent_supports_nested_async_calls_with_future_to_greenlet():
    greenlet = asyncio_gevent.future_to_greenlet(
        asyncio_on_gevent_supports_nested_async_calls_1()
    )
    greenlet.start()
    greenlet.join()
    result = greenlet.get()
    assert result == 42


async def asyncio_on_gevent_supports_nested_async_calls_1():
    result = await asyncio_on_gevent_supports_nested_async_calls_2()
    assert result == 100
    return 42


async def asyncio_on_gevent_supports_nested_async_calls_2():
    result = await asyncio_on_gevent_supports_nested_async_calls_3()
    assert result == "it works"
    return 100


async def asyncio_on_gevent_supports_nested_async_calls_3():
    await asyncio.sleep(1)
    return "it works"


# test asyncio_on_gevent_supports_awaiting_greenlets


def test_asyncio_on_gevent_supports_awaiting_greenlets_with_asyncio_run():
    asyncio.run(asyncio_on_gevent_supports_awaiting_greenlets_1())


def test_asyncio_on_gevent_supports_awaiting_greenlets_with_future_to_greenlet():
    greenlet = asyncio_gevent.future_to_greenlet(
        asyncio_on_gevent_supports_awaiting_greenlets_1()
    )
    greenlet.start()
    greenlet.join()
    result = greenlet.get()
    assert result is None


async def asyncio_on_gevent_supports_awaiting_greenlets_1():
    current_thread = gevent.getcurrent()
    result = await asyncio_gevent.greenlet_to_future(
        gevent.spawn(asyncio_on_gevent_supports_awaiting_greenlets_2, current_thread)
    )
    assert result == 42


def asyncio_on_gevent_supports_awaiting_greenlets_2(parent_thread):
    assert gevent.getcurrent() != parent_thread

    time.sleep(1)

    return 42
