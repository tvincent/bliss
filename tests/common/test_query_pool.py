import pytest
import gevent
from bliss.common.query_pool import NonCooperativeQueryPool

noncooperative_sleep = gevent.monkey.get_original("time", "sleep")


def query(seconds, cooperative=True):
    print(f"sleep for {seconds} seconds ...")
    try:
        if cooperative:
            gevent.sleep(seconds)
        else:
            noncooperative_sleep(seconds)
    except BaseException as e:
        print(f"sleep interrupted: {e}")
        raise
    print("sleep done")


def test_query_success():
    """test executing a query which returns a value"""
    pool = NonCooperativeQueryPool(timeout=0.1)

    def myfunc(a, b=0):
        query(a + b)
        return True

    with gevent.Timeout(5):
        while not pool.execute(myfunc, args=(0.5,), kwargs={"b": 0.5}):
            pass  # every iteration waits for 0.1 seconds


def test_query_exception():
    """test executing a query which raises an exception"""
    pool = NonCooperativeQueryPool(timeout=0.1)

    def myfunc(a, b=0):
        query(a + b)
        raise RuntimeError

    with gevent.Timeout(5):
        with pytest.raises(RuntimeError):
            while not pool.execute(myfunc, args=(0.5,), kwargs={"b": 0.5}):
                pass  # every iteration waits for 0.1 seconds


def test_query_wait():
    """test executing a query which returns a value"""
    pool = NonCooperativeQueryPool(timeout=0.1)

    finished = False

    def myfunc():
        nonlocal finished
        query(1)
        finished = True

    pool.execute(myfunc)

    assert pool.wait(timeout=3), "query did not finish in time"

    assert finished, "query did not finish"


def test_query_maxqueries():
    """test executing a query which returns a value"""
    pool = NonCooperativeQueryPool(timeout=0.1, maxqueries=1)

    def myfunc(a, b=0):
        query(a + b)
        return True

    with gevent.Timeout(5):
        while not pool.execute(myfunc, args=(0.1,), kwargs={"b": 0.1}):
            pass  # every iteration waits for 0.1 seconds
        while not pool.execute(myfunc, args=(0.1,), kwargs={"b": 0.2}):
            pass  # every iteration waits for 0.1 seconds

    previous = pool.execute(myfunc, timeout=0, args=(0.1,), kwargs={"b": 0.2})
    missing = pool.execute(myfunc, timeout=0, args=(0.1,), kwargs={"b": 0.1})
    assert previous, "pool size not large enough"
    assert missing is None, "pool size too large"
    assert pool.wait(timeout=3)

    pool = NonCooperativeQueryPool(timeout=0.1, maxqueries=3)

    with gevent.Timeout(5):
        while not pool.execute(myfunc, args=(0.1,), kwargs={"b": 0.1}):
            pass  # every iteration waits for 0.1 seconds
        while not pool.execute(myfunc, args=(0.1,), kwargs={"b": 0.2}):
            pass  # every iteration waits for 0.1 seconds

    previous = pool.execute(myfunc, timeout=0, args=(0.1,), kwargs={"b": 0.2})
    assert previous, "pool size not large enough"
    previous = pool.execute(myfunc, timeout=0, args=(0.1,), kwargs={"b": 0.1})
    assert previous, "pool size not large enough"
    assert pool.wait(timeout=3)


def test_noncooperative_query_in_noncooperative_pool():
    """test executing a non-cooperative query in a non-cooperative pool (proper usage)"""
    pool = NonCooperativeQueryPool(timeout=0.1)

    call_count = 0

    def myfunc(**kw):
        nonlocal call_count
        call_count += 1
        if call_count > 1:
            # Note: this exception will also be printed when the test passed
            raise RuntimeError("the gevent loop was blocked")
        query(**kw)

    with pytest.raises(gevent.Timeout, match="the gevent loop was not blocked"):
        with gevent.Timeout(0.5, "the gevent loop was not blocked"):
            while not pool.execute(myfunc, kwargs={"seconds": 2, "cooperative": False}):
                pass  # every iteration waits for 0.1 seconds

    assert pool.wait(timeout=4)
