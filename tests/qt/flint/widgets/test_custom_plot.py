"""Testing image plot."""

from bliss.flint.custom_plots import silx_plots


def test_customplot_logbook_send_data(local_flint):
    class IcatClientMockup:
        def __init__(self):
            self.data = None
            self.mimetype = None

        def send_binary_data(self, data: bytes, mimetype: str):
            self.data = data
            self.mimetype = mimetype

    plot = silx_plots.Plot1D()
    icatClient = IcatClientMockup()
    plot.exportToLogbook(icatClient)

    assert b"PNG" in icatClient.data
    assert icatClient.mimetype == "image/png"

    plot.deleteLater()
