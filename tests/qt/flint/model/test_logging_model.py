"""Testing LogWidget."""

import logging
import weakref
import pytest
from bliss.flint.model.logging_model import LoggingModel

test_logger = logging.getLogger(__name__)
test_logger.propagate = False


@pytest.fixture
def loggingModel(silxTestUtils):
    model = LoggingModel()
    try:
        yield model
    finally:
        model.disconnectAll()
        ref = weakref.ref(model)
        model = None
        silxTestUtils.qWaitForDestroy(ref)


def _test_max_logs(xvfb, loggingModel):
    model = loggingModel
    model.setMaximumLogCount(2)
    model.connectLogger(test_logger)
    assert len(model.records()) == 0
    test_logger.warning("A1")
    test_logger.warning("A1")
    test_logger.warning("B2")
    test_logger.warning("B2")
    assert len(model.records()) == 2

    msg = " ; ".join(model.records())
    assert msg.count("A1") == 0
    assert msg.count("B2") == 2


def test_handler_released_on_destroy(xvfb, loggingModel, silxTestUtils):
    model = loggingModel
    nb = len(test_logger.handlers)
    model.connectLogger(test_logger)
    assert len(test_logger.handlers) == nb + 1
    model.disconnectAll()
    assert len(test_logger.handlers) == nb
