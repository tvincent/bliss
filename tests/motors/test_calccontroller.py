# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import numpy
from unittest import mock

from bliss.common.axis import Axis
from bliss.common.standard import ascan, dmesh
from bliss.common.motor_group import Group
from bliss.common import scans
from bliss.common import event


def test_tags(s1ho):
    controller = s1ho.controller
    for tag, axis_name in {
        "front": "s1f",
        "back": "s1b",
        "up": "s1u",
        "down": "s1d",
        "hgap": "s1hg",
        "hoffset": "s1ho",
        "vgap": "s1vg",
        "voffset": "s1vo",
    }.items():
        assert controller._tagged[tag][0].name == axis_name


def test_real_tags(s1ho):
    controller = s1ho.controller
    assert [x.name for x in controller._tagged["real"]] == ["s1f", "s1b", "s1u", "s1d"]


def test_has_tag(s1ho, s1vg, s1u):
    assert s1ho.has_tag("hoffset")
    assert not s1ho.has_tag("vgap")
    assert not s1vg.has_tag("real")


def test_reals_list(s1ho):
    controller = s1ho.controller
    assert len(controller.reals) == 4
    assert all([isinstance(x, Axis) for x in controller.reals])


def test_pseudos_list(s1ho):
    controller = s1ho.controller
    assert len(controller.pseudos) == 4
    assert all([isinstance(x, Axis) for x in controller.pseudos])


def test_exported_pseudo_axes(s1vg, s1vo, s1hg, s1ho):
    assert all((s1vg, s1vo, s1hg, s1ho))
    controller = s1vg.controller
    assert all((axis.controller == controller for axis in (s1vg, s1vo, s1hg, s1ho)))
    assert all(["READY" in axis.state for axis in controller.pseudos])


def test_real_axis_is_right_object(s1f, s1ho, m1):
    controller = s1ho.controller
    assert s1f == controller.axes["s1f"]
    assert s1f.controller == m1.controller


def test_calc_with_init(roby, calc_mot1):
    # see issue #488
    # calc_mot1 defines some attribute in 'initialize_axis',
    # the next two lines should pass without exception:
    roby.position
    roby.sync_hard()


def test_limits(default_session, s1hg):
    # ======== Test a simple calc (reals are real axes) ================
    with pytest.raises(ValueError):
        s1hg.move(40)
    with pytest.raises(ValueError):
        s1hg.move(-16)

    # ======== Test a calc of calc (reals are pseudo axes) ============
    robz = default_session.config.get("robz")
    theta = default_session.config.get("theta")
    twotheta = default_session.config.get("twotheta")

    robz_backlash = robz.backlash
    robz_limits = robz.limits

    robz.backlash = 0
    robz.limits = [0, 1]
    theta.limits = [-90, 90]
    twotheta.limits = [-180, 180]

    # === generated positions
    # twotheta -180  0  180
    # theta     -90  0  90
    # robz        0  1  0

    twotheta_positions = numpy.array([-180, 0, 180])

    theta_positions = twotheta.controller.calc_to_real(
        {"twotheta": twotheta_positions}
    )["theta"]
    assert theta_positions == pytest.approx([-90, 0, 90])

    robz_positions = theta.controller.calc_to_real({"angle": theta_positions})["px"]
    assert robz_positions == pytest.approx([0, 1, 0])

    # ========== It should not raise an error ==========================
    for pos in twotheta_positions:
        twotheta.move(pos)
    twotheta.controller.check_limits((twotheta, twotheta_positions))

    # ========== It should raise an error because of robz limits =======
    robz.limits = [0, 0.5]
    for pos in [0]:
        with pytest.raises(ValueError) as excinfo:
            twotheta.move(pos)
        assert excinfo.value.args[0].startswith("robz:")
    with pytest.raises(ValueError) as excinfo:
        twotheta.controller.check_limits((twotheta, twotheta_positions))
    assert excinfo.value.args[0].startswith("robz:")
    with pytest.raises(ValueError) as excinfo:
        ascan(twotheta, -90, 90, 2, 0.1, run=False)
    assert excinfo.value.args[0].startswith("robz:")
    robz.limits = [0, 1]

    # ========== It should raise an error because of theta limits ======
    theta.limits = [-80, 80]
    for pos in [-180, 180]:
        with pytest.raises(ValueError) as excinfo:
            twotheta.move(pos)
        assert excinfo.value.args[0].startswith("theta:")
    with pytest.raises(ValueError) as excinfo:
        twotheta.controller.check_limits((twotheta, twotheta_positions))
    assert excinfo.value.args[0].startswith("theta:")
    theta.limits = [-90, 90]

    # ========== It should raise an error because of twotheta limits ===
    twotheta.limits = [-100, 100]
    for pos in [-180, 180]:
        with pytest.raises(ValueError) as excinfo:
            twotheta.move(pos)
        assert excinfo.value.args[0].startswith("twotheta:")
    with pytest.raises(ValueError) as excinfo:
        twotheta.controller.check_limits((twotheta, twotheta_positions))
    assert excinfo.value.args[0].startswith("twotheta:")
    twotheta.limits = [-180, 180]
    # ===================================================================

    robz.limits = robz_limits
    robz.backlash = robz_backlash


def test_hw_limits_and_set_pos(s1f, s1b, s1hg):
    try:
        s1f.controller.set_hw_limits(s1f, -2, 2)
        s1b.controller.set_hw_limits(s1b, -2, 2)
        with pytest.raises(RuntimeError):
            s1hg.move(6)
        assert s1hg._set_position == pytest.approx(s1hg.position)
    finally:
        s1f.controller.set_hw_limits(s1f, None, None)
        s1b.controller.set_hw_limits(s1b, None, None)


def test_real_move_and_set_pos(s1f, s1b, s1hg):
    s1hg.move(0.5)
    s1f.rmove(1)
    s1b.rmove(1)
    assert s1f._set_position == pytest.approx(1.25)
    assert s1b._set_position == pytest.approx(1.25)
    assert s1hg.position == pytest.approx(2.5)
    assert s1hg._set_position == pytest.approx(2.5)


def test_set_dial(roby, calc_mot1):
    calc_mot1.move(4)
    assert roby.position == pytest.approx(2)
    calc_mot1.dial = 0
    assert calc_mot1.position == pytest.approx(4)
    assert calc_mot1.dial == pytest.approx(0)
    assert roby.position == pytest.approx(0)


def test_set_position(roby, calc_mot1):
    calc_mot1.move(1)
    assert calc_mot1.offset == pytest.approx(0)
    assert roby.position == pytest.approx(0.5)
    calc_mot1.position = 0
    assert calc_mot1.offset == pytest.approx(-1)
    assert calc_mot1.position == pytest.approx(0)
    assert calc_mot1.dial == pytest.approx(1)
    assert roby.position == pytest.approx(0.5)


def test_offset_set_position(calc_mot1):
    calc_mot1.dial = 0
    calc_mot1.position = 1
    assert calc_mot1._set_position == pytest.approx(1)
    calc_mot1.move(0.1)
    assert calc_mot1._set_position == pytest.approx(0.1)


def test_calc_in_calc(roby, calc_mot1, calc_mot2):
    calc_mot1.move(1)
    assert calc_mot1.position == pytest.approx(1)
    assert roby.position == pytest.approx(0.5)
    calc_mot2.move(1)
    assert calc_mot1.position == pytest.approx(0.5)
    assert calc_mot2.position == pytest.approx(1)
    assert roby.position == pytest.approx(0.25)


def test_ascan_limits(session, s1hg, s1f, s1b):
    s1hg.position = 0
    s1hg.dial = 0
    s1f.limits = -1, 1
    s1b.limits = -1, 1
    with pytest.raises(ValueError) as out_of_range_exc:
        s1hg.move(2.1)
    assert "would exceed high limit" in str(out_of_range_exc.value)
    with pytest.raises(ValueError) as out_of_range_exc:
        ascan(s1hg, -1, 2.1, 10, 0.1, run=False)
    assert "would exceed high limit" in str(out_of_range_exc.value)


def test_same_calc_real_grp_move(s1hg, s1f, roby, calc_mot2):
    # test for issue 481
    with pytest.raises(RuntimeError) as exc:
        Group(s1hg, s1f)

    assert (
        "Virtual axis 's1hg` cannot be present in group with any of its corresponding real axes: ['s1f']"
        in str(exc.value)
    )

    with pytest.raises(RuntimeError) as exc:
        Group(roby, calc_mot2)

    assert (
        "Virtual axis 'calc_mot1` cannot be present in group with any of its corresponding real axes: ['roby']"
        in str(exc.value)
    )


def test_calc_motor_publishing(session, calc_mot2):
    diode = session.config.get("diode")
    m0 = session.config.get("m0")

    s = scans.a2scan(calc_mot2, 0, 1, m0, 0, 1, 3, 0.1, diode, save=False)
    pub_motors = s.scan_info["acquisition_chain"]["axis"]["master"]["scalars"]

    assert "axis:calc_mot2" in pub_motors
    assert "axis:m0" in pub_motors
    assert "axis:calc_mot1" in pub_motors
    assert "axis:roby" in pub_motors


def test_calc_motor_energy(beacon):
    # calculate energy form the position of roby
    mono = beacon.get("mono")
    mono.position = 9.1

    energy = beacon.get("energy")
    assert abs(energy.position - 12.5011) < 0.001

    wavelength = beacon.get("wavelength")
    assert abs(wavelength.position - 0.991784) < 0.001

    energy.move(6.56)
    assert abs(mono.position - 17.54141) < 0.001
    energy.controller.close()


def test_calc_motor_no_settings_axis(beacon, calc_mot3, nsa):
    # calc_mot3.position == 2 * nsa.position
    nsa.move(1)
    assert calc_mot3.position == pytest.approx(2)
    calc_mot3.move(1)
    assert nsa.position == pytest.approx(0.5)


def test_issue_1909(session):
    llbragg1 = session.config.get("llbragg1")
    llbeamy1 = session.config.get("llbeamy1")
    diode = session.config.get("diode")

    dmesh(llbragg1, -0.005, 0.005, 3, llbeamy1, -1, 1, 2, 0.01, diode)


def test_calc_sync_hard(session, calc_mot1, roby):
    assert roby.position == 0

    # change roby position directly in controller
    # (like moving motor manually),
    # then check that pseudo pos is updated when
    # calling sync_hard on real axis
    roby.controller.set_hw_position(roby, 1 * roby.steps_per_unit)
    roby.sync_hard()
    assert roby.position == 1
    assert calc_mot1.position == 2

    # test issue #3091
    # change roby position, without calc_mot1 noticing
    event.disconnect(
        roby, "internal_position", calc_mot1.controller._real_position_update
    )
    event.disconnect(
        roby, "internal__set_position", calc_mot1.controller._real_setpos_update
    )
    roby.position = 2
    roby.dial = 2  # make no offset
    event.connect(roby, "internal_position", calc_mot1.controller._real_position_update)
    event.connect(
        roby, "internal__set_position", calc_mot1.controller._real_setpos_update
    )

    assert roby.position == 2
    assert calc_mot1.position == 2
    calc_mot1.sync_hard()
    assert calc_mot1.position == 4

    # change roby position directly in controller
    # (like moving motor manually),
    # then do sync_hard on pseudo axis and check
    # real axis is updated
    roby.controller.set_hw_position(roby, 1 * roby.steps_per_unit)
    assert roby.position == 2
    assert calc_mot1.position == 4
    calc_mot1.sync_hard()
    assert roby.position == 1
    assert calc_mot1.position == 2


def test_calc_with_bad_real(default_session):
    bad = default_session.config.get("bad")
    calc = default_session.config.get("coupled_calc2")
    assert calc.state == "READY"
    calc.sync_hard()
    assert calc.state == "READY"

    bad.controller.fault_state = True
    calc.sync_hard()
    assert calc.state == "FAULT"


def test_calc_pseudo_sync(default_session):
    """Check there is no discrepancy between pseudo.position and pseudo._set_position after
    a parameter used in calc_from_real has been changed (the parameter is not an axis).
    """
    xpos = default_session.config.get("rcosx")  # pseudo
    omega = default_session.config.get("omega")  # real
    ctrl = xpos.controller  # calc_controller

    # set radius, move and check no discrepancy
    ctrl.radius = 2
    omega.move(0)
    assert xpos.position == 2
    assert xpos._set_position == 2

    # set radius and check no discrepancy
    ctrl.radius = 1
    assert xpos.position == 1
    assert xpos._set_position == 1

    # rmove and check no discrepancy
    omega.rmove(180)
    assert xpos.position == -1
    assert xpos._set_position == -1

    # set radius and check no discrepancy
    ctrl.radius = 2
    assert xpos.position == -2
    assert xpos._set_position == -2

    # rmove and check no discrepancy
    omega.rmove(-180)
    assert xpos.position == 2
    assert xpos._set_position == 2


def test_calcmot_w_offset_scan_check_limits_issue_3239(session, calc_mot1):
    diode = session.config.get("diode")
    calc_mot1.offset = 2

    with mock.patch.object(
        calc_mot1.controller, "check_limits", wraps=calc_mot1.controller.check_limits
    ) as mocked_check_limits:
        ascan(calc_mot1, 0, 1, 5, 0.01, diode)
        mocked_check_limits.assert_called_once()
        # cannot use 'assert_called_with' because of numpy array comparison
        call_items = mocked_check_limits.call_args[0][0]
        axis, pos = call_items
        assert axis is calc_mot1
        numpy.testing.assert_array_equal(
            pos, numpy.array([-2, -1.8, -1.6, -1.4, -1.2, -1])
        )


def test_issue_3285(session, robz):
    diode = session.config.get("diode")
    robz.offset = -35
    robz.limits = -5, 5
    scans.ascan(robz, -4, -3, 3, 0.1, diode)
    with pytest.raises(ValueError) as exc:
        scans.ascan(robz, -6, -3, 3, 0.1, diode)
    assert "would exceed low limit" in str(exc)
    with pytest.raises(ValueError) as exc:
        scans.ascan(robz, -3, 6, 3, 0.1, diode)
    assert "would exceed high limit" in str(exc)


def test_check_limits(session, simulation_monochromator):
    mono = simulation_monochromator
    sim_mono1 = mono.motors.bragg
    sim_mono1e = mono.motors.energy
    ### put same conditions as on ID09
    sim_mono1.sign = -1
    sim_mono1.dial = 44.3499
    sim_mono1.offset = 51.4479
    sim_mono1.limits = 1.3, 29.8
    ###
    scans.dscan(sim_mono1e, -3, 3, 1, 0)


def test_issue_3454(session, s1u, s1d, s1vg, s1vo):
    """
    3454-problem-in-limit-check-of-calc-motors-when-scalars-and-arrays-are-mixed-in-positions-dict

    This test is to ensure that positions dict passed to a calc controller
    contains only numpy array in limit check.
    """
    diode = session.config.get("diode")

    with mock.patch.object(
        s1vg.controller, "calc_to_real", wraps=s1vg.controller.calc_to_real
    ) as mocked_calc_to_real:
        scans.dscan(s1vg, 0, 1, 3, 0.1, diode)
        # check first call (= limits check) is done with numpy arrays
        limit_check_call_args = mocked_calc_to_real.call_args_list[0]
        pos_dict = limit_check_call_args[0][0]
        for axis_name in pos_dict:
            position = pos_dict[axis_name]
            # print("axis_name=", axis_name, "pos=", pos_dict[axis_name])
            assert isinstance(position, numpy.ndarray)
