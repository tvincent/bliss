# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import os
import re
import glob
import jedi
import logging
import random
from unittest import mock
from bliss.common.logtools import Elogbook
from bliss.shell.standard import elog_add
from bliss.common.utils import autocomplete_property, UserNamespace
import bliss.shell.cli
from bliss.common.greenlet_utils import asyncio_gevent
import pytest
import gevent

from bliss.shell.cli.repl import PromptToolkitOutputWrapper
from .conftest import bliss_repl, _feed_cli_with_input


def run_repl_once(bliss_repl, text):
    bliss_repl.app.input.send_text(text)
    try:
        res = bliss_repl.eval(bliss_repl.app.run())
    except KeyboardInterrupt:  # KeyboardInterrupt doesn't inherit from Exception.
        raise
    except SystemExit:
        return
    except BaseException as e:
        bliss_repl._handle_exception(e)
    else:
        if res is not None:
            bliss_repl.show_result(res)


def test_shell_exit():
    try:
        _feed_cli_with_input(chr(0x4) + "y", check_line_ending=False)
    except EOFError:
        assert True


def test_shell_exit2():
    try:
        _feed_cli_with_input(chr(0x4) + "\r", check_line_ending=False)
    except EOFError:
        assert True


def test_shell_noexit():
    result, cli, br = _feed_cli_with_input(
        chr(0x4) + "nprint 1 2\r", check_line_ending=True, confirm_exit=True
    )
    assert result == "print(1,2)"


@pytest.mark.parametrize(
    "delay", (0,) + tuple(random.randint(0, 5) / 10 for i in range(3))
)
def test_shell_kbint_during_move(beacon, delay):
    roby = beacon.get("roby")
    with bliss_repl({"roby": roby}) as br:
        br.app.input.send_text("roby.move(1000)\r")
        g = asyncio_gevent.future_to_greenlet(br.run_async())
        while roby.state.READY:
            gevent.sleep(0.005)
        gevent.sleep(delay)
        assert roby.state.MOVING
        killpos = roby.position
        assert br._current_eval_g
        br._current_eval_g.kill(KeyboardInterrupt)
        br.app.input.send_text(chr(0x4))  # ctrl-d
        g.join()
        assert roby.state.READY
        assert roby.position < 1000, f"KILL POS WAS {killpos}"


def test_shell_ctrl_r():

    result, cli, br = _feed_cli_with_input(
        chr(0x12) + "bla blub\r\r", check_line_ending=True
    )
    assert result == ""

    result, cli, br = _feed_cli_with_input(
        "from bliss import setup_globals\rfrom subprocess import Popen\r"
        + chr(0x12)
        + "from bl\r\r",
        check_line_ending=True,
    )
    assert result == "from bliss import setup_globals"


def test_shell_comma_backets():
    result, cli, _ = _feed_cli_with_input("print 1 2\r")
    assert result == "print(1,2)"


def test_shell_string_input():
    result, cli, _ = _feed_cli_with_input("a='to to'\r")
    assert result == "a='to to'"


def test_shell_string_parameter():
    result, cli, _ = _feed_cli_with_input("print 'bla bla'\r")
    assert result == "print('bla bla')"


def test_shell_function_without_parameter():
    result, cli, _ = _feed_cli_with_input("print\r")
    assert result == "print"

    def f():
        pass

    result, cli, _ = _feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f()"


def test_shell_function_with_return_only():
    result, cli, _ = _feed_cli_with_input("\r")
    assert result == ""


def test_shell_callable_with_args():
    result, cli, _ = _feed_cli_with_input("sum\r")
    assert result == "sum"

    def f(arg):
        pass

    result, cli, _ = _feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f"


def test_shell_callable_with_kwargs_only():
    result, cli, _ = _feed_cli_with_input("property\r")
    assert result == "property()"

    def f(arg="bla"):
        pass

    result, cli, _ = _feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f()"


def test_shell_callable_with_args_and_kwargs():
    result, cli, _ = _feed_cli_with_input("compile\r")
    assert result == "compile"

    def f(arg, kwarg="bla"):
        pass

    result, cli, _ = _feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f"


def test_shell_list():
    result, cli, _ = _feed_cli_with_input("list\r")
    assert result == "list"

    lst = list()
    result, cli, _ = _feed_cli_with_input("lst\r", locals_dict={"lst": lst})
    assert result == "lst"


def test_shell_ScanSaving(beacon):
    from bliss.scanning.scan_saving import ScanSaving

    s = ScanSaving()

    result, cli, _ = _feed_cli_with_input("s\r", locals_dict={"s": s})
    assert result == "s"


def test_shell_func():
    def f():
        pass

    result, cli, _ = _feed_cli_with_input("f\r", locals_dict={"f": f})
    assert result == "f()"


def test_shell_semicolon():
    result, cli, _ = _feed_cli_with_input("print 1 2;print 1\r")
    assert result == "print(1,2);print(1)"
    result, cli, _ = _feed_cli_with_input("print 1 2;print 1;print 23\r")
    assert result == "print(1,2);print(1);print(23)"


def test_shell_comma_outside_callable_assignment():
    result, cli, _ = _feed_cli_with_input("a=True \r")
    assert result == "a=True"


def test_shell_comma_outside_callable_bool():
    result, cli, _ = _feed_cli_with_input("True \r")
    assert result == "True"


def test_shell_comma_outside_callable_string():
    result, cli, _ = _feed_cli_with_input("'bla' \r")
    assert result == "'bla'"


def test_shell_comma_outside_callable_number():
    result, cli, _ = _feed_cli_with_input("1.1 + 1  \r")
    assert result == "1.1 + 1"


def test_shell_comma_after_comma():
    result, cli, _ = _feed_cli_with_input("1, \r")
    assert result == "1,"


def test_protected_against_trailing_whitespaces():
    """Check that the number of spaces (N) after a command doesn't make the command to be repeated N-1 times"""

    result, cli, br = _feed_cli_with_input(
        f"f=lambda: print('Om Mani Padme Hum'); f() {' '*5}\r"
    )

    output = cli.output
    br.eval(result)

    assert output[-1].strip() == "Om Mani Padme Hum"


def test_info_dunder():
    class A:
        def __repr__(self):
            return "repr-string"

        def __str__(self):
            return "str-string"

        def __info__(self):
            return "info-string"

        def titi(self):
            return "titi-method"

    class B:
        def __repr__(self):
            return "repr-string"

    class C:
        pass

    # '__info__()' method called at object call.
    with bliss_repl({"A": A(), "B": B(), "C": C()}) as br:
        run_repl_once(br, "A\r")
        assert "info-string" in br.app.output[-1]

        run_repl_once(br, "[A]\r")
        assert "[repr-string]" in br.app.output[-1]

        # 2 parenthesis added to method if not present
        run_repl_once(br, "A.titi\r")
        assert "titi-method" in br.app.output[-1]

        # Closing parenthesis added if only opening one is present.
        run_repl_once(br, "A.titi(\r")
        assert "titi-method" in br.app.output[-1]

        # Ok if finishing by a closing parenthesis.
        run_repl_once(br, "A.titi()\r")
        assert "titi-method" in br.app.output[-1]

        # '__repr__()' used if no '__info__()' method is defined.
        run_repl_once(br, "B\r")
        assert "repr-string" in br.app.output[-1]

        # Default behaviour for object without specific method.
        run_repl_once(br, "C\r")
        assert "C object at " in br.app.output[-1]

    bliss.shell.cli.typing_helper_active = False
    try:
        with bliss_repl({"A": A, "B": B, "A.titi": A.titi}) as br:
            output = br.app.output
            run_repl_once(br, "A\r")
            assert (
                "<class 'tests.shell.test_bliss_shell_basics.test_info_dunder.<locals>.A'>\r\n\n"
                == output[-1]
            )
    finally:
        bliss.shell.cli.typing_helper_active = True


def test_shell_dict_list_not_callable():
    result, cli, _ = _feed_cli_with_input("d \r", locals_dict={"d": dict()})
    assert result == "d"


def test_property_evaluation():
    class Bla:
        def __init__(self):
            self.i = 0

        @property
        def test(self):
            self.i += 1
            return self.i

    b = Bla()

    result, cli, _ = _feed_cli_with_input("b.test     \r", locals_dict={"b": b})
    assert b.test == 1
    result, cli, _ = _feed_cli_with_input("b.test;print 1\r", locals_dict={"b": b})
    result, cli, _ = _feed_cli_with_input("b.test;print 1\r", locals_dict={"b": b})
    result, cli, _ = _feed_cli_with_input("b.test;print 1\r", locals_dict={"b": b})
    assert b.test == 2


def test_func_no_args():
    result, cli, _ = _feed_cli_with_input("f \r", locals_dict={"f": lambda: None})
    assert result == "f()"


def _repl_out_to_string(out):
    ansi_escape = re.compile(r"\x1B[@-_][0-?]*[ -/]*[@-~]")
    return ansi_escape.sub("", out)


def test_nested_property_evaluation():
    class A:
        def __init__(self):
            self.count = 0

        @property
        def foo(self):
            self.count += 1
            return self.count

    class B:
        def __init__(self):
            self.a = A()

        @property
        def bar(self):
            return self.a

    b = B()

    result, cli, _ = _feed_cli_with_input("b.bar.foo\r", locals_dict={"b": b})
    result, cli, _ = _feed_cli_with_input("b.bar.foo\r", locals_dict={"b": b})
    assert b.bar.foo == 1


def test_deprecation_warning(beacon, capfd, log_context):
    def test_deprecated():
        print("bla")
        from bliss.common.deprecation import deprecated_warning

        deprecated_warning(
            kind="function",
            name="ct",
            replacement="sct",
            reason="`ct` does no longer allow to save data",
            since_version="1.5.0",
            skip_backtrace_count=5,
            only_once=False,
        )

    with bliss_repl({"func": test_deprecated}) as br:
        br.app.input.send_text("func()\r")
        result = br.app.run()
        br.eval(result)
        captured = capfd.readouterr()

    out = _repl_out_to_string(captured.out)
    err = _repl_out_to_string(captured.err)
    assert "bla" in out
    assert "Function ct is deprecated since" in err


def test_captured_output():
    def decorated_cell_output(s):
        return f"{s}\r\n\n"

    with bliss_repl() as br:
        br.raw_eval("f=lambda num: print(num+1) or num+2\r")
        br.raw_eval("fnone=lambda: None\r")
        br.raw_eval("fnoneprint=lambda: print('hello\\nworld')\r")
        output = br.app.output

        run_repl_once(br, "f(1)\r")
        run_repl_once(br, "f(3)\r")
        run_repl_once(br, "fnone()\r")
        run_repl_once(br, "f(4)\r")
        run_repl_once(br, "fnoneprint()\r")
        run_repl_once(br, "f(5)\r")

        assert output[1] == decorated_cell_output("2\n3")
        assert output[2] == decorated_cell_output("4\n5")
        assert output[3] is None
        assert output[4] == decorated_cell_output("5\n6")
        assert output[5] == "hello\nworld\n"
        assert output[6] == decorated_cell_output("6\n7")

        assert output[-1] == decorated_cell_output("6\n7")
        assert output[-2] == "hello\nworld\n"
        assert output[-3] == decorated_cell_output("5\n6")
        assert output[-4] is None
        assert output[-5] == decorated_cell_output("4\n5")
        assert output[-6] == decorated_cell_output("2\n3")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [1]")
        ):
            output[-7]
        with pytest.raises(IndexError, match=re.escape("the last cell is OUT [6]")):
            output[7]
        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [1]")
        ):
            output[0]

        # Fill the output history with None's
        MAXLEN = output._MAXLEN
        for _ in range(MAXLEN - 6):
            run_repl_once(br, "fnone()\r")

        assert output[1] == decorated_cell_output("2\n3")

        # Push the first entry out of the history
        run_repl_once(br, "fnone()\r")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [2]")
        ):
            output[1]

        # Push the second entry out of the history
        run_repl_once(br, "fnone()\r")

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            output[1]

        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            output[-MAXLEN - 3]
        with pytest.raises(
            IndexError, match=re.escape(f"the last cell is OUT [{MAXLEN+2}]")
        ):
            output[MAXLEN + 3]
        with pytest.raises(
            IndexError, match=re.escape("the first available cell is OUT [3]")
        ):
            output[0]


def test_elogbook_elog_add(beacon):
    def return_arg(s):
        return s

    results = [None, "hello", None, "hello\nworld"]

    def assert_elog_add(idx, *expected_args, **expected_kw):
        nonlocal results, br
        with mock.patch.object(
            Elogbook, "comment", return_value=None
        ) as mock_elogbook_comment:
            if idx is None:
                idx = -1
                run_repl_once(br, "elog_add()\r")
            else:
                run_repl_once(br, f"elog_add({idx})\r")

        if not expected_args:
            # The output of this cell is empty
            mock_elogbook_comment.assert_not_called()
        else:
            # The output of this cell is not empty
            mock_elogbook_comment.assert_called_once_with(*expected_args, **expected_kw)
        results.append(None)  # output of the elog_add call itself

    def decorated_cell_output(s):
        return f"{repr(s)}\r\n\n"

    repl_locals = {"return_arg": return_arg, "elog_add": elog_add}
    with bliss_repl(repl_locals) as br:
        br.initialize_session()

        for s in results:
            if s is None:
                run_repl_once(br, "return_arg(None)\r")
            else:
                run_repl_once(br, f"return_arg({repr(s)})\r")

        # `elog_add()` logs the last output by default
        assert_elog_add(None, decorated_cell_output("hello\nworld"), beamline_only=None)

        # `elog_add(i)` index should correspond to the cell index
        assert_elog_add(1)
        assert_elog_add(2, decorated_cell_output("hello"), beamline_only=None)
        assert_elog_add(3)
        assert_elog_add(4, decorated_cell_output("hello\nworld"), beamline_only=None)

        idx = 4 - len(results) - 1
        assert results[idx] == "hello\nworld"
        assert_elog_add(idx, decorated_cell_output("hello\nworld"), beamline_only=None)

        idx = 2 - len(results) - 1
        assert results[idx] == "hello"
        assert_elog_add(idx, decorated_cell_output("hello"), beamline_only=None)


def test_elogbook_cmd_log_and_elog_add(beacon):
    logger = logging.getLogger()

    with bliss_repl({"logger": logger, "elog_add": elog_add}) as br:
        br.initialize_session()
        br.raw_eval(
            """def f(num):
          print(num+1)\r
          logger.info("my info")\r
          logger.error("my error")\r
          return num+2\r"""
        )

        # calling when no command has been issued yet should not raise exception
        elog_add()

        with mock.patch.object(
            Elogbook, "command", return_value=None
        ) as mock_elogbook_command:
            run_repl_once(br, "f(1)\r")

            output = br.app.output
            captured = output[-1]

        mock_elogbook_command.assert_called_once_with("f(1)")
        assert captured == "2\n3\r\n\n"

        with mock.patch.object(
            Elogbook, "comment", return_value=None
        ) as mock_elogbook_comment:
            run_repl_once(br, "elog_add()\r")

        mock_elogbook_comment.assert_called_once_with(captured, beamline_only=None)

        with mock.patch.object(
            Elogbook, "error", return_value=None
        ) as mock_elogbook_error:
            run_repl_once(br, "1/0\r")

        mock_elogbook_error.assert_called_once_with(
            "ZeroDivisionError: division by zero"
        )

        with mock.patch.object(
            Elogbook, "comment", return_value=None
        ) as mock_elogbook_comment:
            run_repl_once(br, "elog_add()\r")

        # The last cell raised a ZeroDivisionError so there
        # is no output for the logbook.
        mock_elogbook_comment.assert_not_called()


def test_getattribute_evaluation():
    n = None

    class A:
        def __init__(self):
            global n
            n = 0

        def __getattribute__(self, value):
            global n

            if n < 1:
                n += 1
                raise RuntimeError
            return 0

    a = A()

    with gevent.timeout.Timeout(3):
        result, cli, _ = _feed_cli_with_input("a.foo()\r", locals_dict={"a": a})


def test_excepthook():
    with bliss_repl() as br:
        with mock.patch.object(
            PromptToolkitOutputWrapper, "write", return_value=None
        ) as mock_output_write:
            br.eval("raise RuntimeError('excepthook test')\r")

    mock_output_write.assert_has_calls(
        (
            mock.call(
                "!!! === RuntimeError: excepthook test === !!! ( for more details type cmd 'last_error(0)' )"
            ),
            mock.call("\n"),
        )
    )


def test_history_archiving(tmpdir):
    history_filename = tmpdir / ".bliss_pytest_dummy_history"

    # ensure there exist no such history files before testing
    for p in glob.glob(f"{history_filename}*"):
        os.remove(p)

    def dummy_entry(id):
        return f"# 1970-01-01 12:00:{id}.000000\n+dummy_cmd_{id}()\n\n"

    # create a fifty entries history file
    history = "".join([dummy_entry(i) for i in range(50)])
    with open(history_filename, "w") as f:
        f.write(history)

    # file size < threshold : no archiving
    bliss.shell.cli.repl._archive_history(history_filename, file_size_thresh=3000)
    with open(history_filename, "r") as f:
        assert f.read() == history

    # file size > threshold : archiving
    bliss.shell.cli.repl._archive_history(
        history_filename, file_size_thresh=2000, keep_active_entries=15
    )
    with open(history_filename, "r") as f:
        assert f.read() == "".join([dummy_entry(i) for i in range(50 - 15, 50)])

    archive_file = glob.glob(f"{history_filename}_*")[0]
    with open(archive_file, "r") as f:
        assert f.read() == "".join([dummy_entry(i) for i in range(50 - 15)])


def test_autocomplete_property(capsys):
    class A:
        @property
        def foo(self):
            print("FOO")

    class B:
        @autocomplete_property
        def bar(self):
            return A()

        @property
        def bar2(self):
            print("BAR2")
            return A()

        @autocomplete_property
        def toto(self):
            print("TOTO")

    b = B()

    i = jedi.Interpreter("b.bar.", path="input-text", namespaces=[locals(), globals()])
    completions = [c.complete for c in i.complete()]
    assert "foo" in completions

    i = jedi.Interpreter("b.bar2.", path="input-text", namespaces=[locals(), globals()])
    completions = [c.complete for c in i.complete()]
    assert "foo" not in completions

    i = jedi.Interpreter("b.toto.", path="input-text", namespaces=[locals(), globals()])
    completions = [c.complete for c in i.complete()]

    output = capsys.readouterr().out
    assert "TOTO" in output
    assert "BAR2" not in output
    assert "FOO" not in output

    un = UserNamespace({"b": b})
    i = jedi.Interpreter("un.b.", path="input-text", namespaces=[locals(), globals()])
    completions = [c.complete for c in i.complete()]
    assert "bar" in completions
    assert "bar2" in completions
    assert "toto" in completions
