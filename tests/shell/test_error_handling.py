# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from datetime import date as datetime
from pygments import lex
from pygments.token import Token
from bliss.shell.cli.formatted_traceback import BlissTracebackLexer

from .conftest import bliss_repl


def test_error_report_chained():
    with bliss_repl() as br:
        error_report = br.error_report
        br.eval(
            """
def func1():
    raise RuntimeError("LEVEL 0")
def func2():
    try:
        func1()
    except Exception as e:
         raise RuntimeError("LEVEL 1") from e
"""
        )
        br.eval("func2()\r")

    assert len(error_report) == 1
    last_error = error_report[-1].format()
    assert "\nRuntimeError: LEVEL 0" in last_error
    assert "\nRuntimeError: LEVEL 1" in last_error
    assert datetime.today().strftime("%d/%m/%Y") in last_error


@pytest.mark.parametrize("show_locals", (True, False))
def test_error_report_format(show_locals):
    with bliss_repl() as br:
        error_report = br.error_report
        br.eval(
            """
def func1():
    # variable longer than max_local_len
    longvar = "bar" * 100  # noqa: F841
    raise RuntimeError("LEVEL 0")

def func2():
    # number of variables exceeding max_nb_locals
    a = 0  # noqa: F841
    b = 0  # noqa: F841
    c = 0  # noqa: F841
    d = 0  # noqa: F841
    try:
        func1()
    except Exception as e:
        # will cause "The above exception was the direct cause..." message
        raise RuntimeError("LEVEL 1") from e

def func3():
    foo = 42  # noqa: F841
    try:
        func2()
    except Exception:
        # will cause "During handling of the above..." message
        raise RuntimeError("LEVEL 2")
"""
        )
        br.eval("func3()\r")

    assert len(error_report) == 1
    formatted_error = error_report[-1].format(
        max_nb_locals=3,
        max_local_len=50,
        show_locals=show_locals,
        filter_from_filename=None,
    )
    lines = formatted_error.splitlines()

    assert lines[0].startswith(datetime.today().strftime("%d/%m/%Y"))
    assert lines[1] == "Traceback (most recent call last):"
    assert (
        "During handling of the above exception, another exception occurred:" in lines
    )
    assert (
        "The above exception was the direct cause of the following exception:" in lines
    )
    if show_locals:
        assert "    @foo: 42" in lines
        assert "    @longvar: ... (truncated)" in lines
        assert "    ... (truncated)" in lines
    else:
        assert "    @foo: 42" not in lines
    assert "RuntimeError: LEVEL 0" in lines
    assert "RuntimeError: LEVEL 1" in lines
    assert "RuntimeError: LEVEL 2" in lines


# =================== TRACEBACK LEXER TESTS ===================
# Following tests are only written for non-regression purpose,
# as there is no specific requirements on coloration.


def test_error_report_lexer():
    tb = """\
01/01/1970 01:02:03
Traceback (most recent call last):
  File "filename", line 1546, in func
    line of code
    @foo: 'bar'
RuntimeError: something happened
"""
    tokens = list(lex(tb, lexer=BlissTracebackLexer()))
    expected_tokens = [
        (Token.Name.Property, "01/01/1970 01:02:03\n"),
        (Token.Text, "Traceback (most recent call last):\n"),
        (Token.Text, "  File "),
        (Token.Name.Builtin, '"filename"'),
        (Token.Text, ", line "),
        (Token.Literal.Number, "1546"),
        (Token.Text, ", in "),
        (Token.Name, "func"),
        (Token.Text, "\n"),
        (Token.Text, "    "),
        (Token.Other, "line of code"),
        (Token.Text, "\n"),
        (Token.Text, "    "),
        (Token.Name.Variable, "@foo"),
        (Token.Punctuation, ":"),
        (Token.Literal.String.Other, " 'bar'"),
        (Token.Text, "\n"),
        (Token.Generic.Error, "RuntimeError"),
        (Token.Text, ": "),
        (Token.Name.Exception, "something happened"),
        (Token.Text, "\n"),
    ]
    assert tokens == expected_tokens


def test_error_report_lexer_locals_truncated():
    tb = """\
01/01/1970 01:02:03
Traceback (most recent call last):
  File "filename", line 1546, in func
    line of code
    @foo: ... (truncated)
    ... (truncated)
RuntimeError: something happened
"""
    tokens = list(lex(tb, lexer=BlissTracebackLexer()))
    expected_tokens = [
        (Token.Name.Property, "01/01/1970 01:02:03\n"),
        (Token.Text, "Traceback (most recent call last):\n"),
        (Token.Text, "  File "),
        (Token.Name.Builtin, '"filename"'),
        (Token.Text, ", line "),
        (Token.Literal.Number, "1546"),
        (Token.Text, ", in "),
        (Token.Name, "func"),
        (Token.Text, "\n"),
        (Token.Text, "    "),
        (Token.Other, "line of code"),
        (Token.Text, "\n"),
        (Token.Text, "    "),
        (Token.Name.Variable, "@foo"),
        (Token.Punctuation, ": "),
        (Token.Comment.Preproc, "... (truncated)\n"),
        (Token.Comment.Preproc, "    ... (truncated)\n"),
        (Token.Generic.Error, "RuntimeError"),
        (Token.Text, ": "),
        (Token.Name.Exception, "something happened"),
        (Token.Text, "\n"),
    ]
    assert tokens == expected_tokens


def test_error_report_lexer_chained():
    tb = """\
01/01/1970 01:02:03
Traceback (most recent call last):
  File "filename", line 1546, in func
RuntimeError: something happened

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "filename", line 1546, in func
RuntimeError: something happened

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
  File "filename", line 1546, in func
RuntimeError: something happened
"""
    tokens = list(lex(tb, lexer=BlissTracebackLexer()))
    expected_tokens = [
        (Token.Name.Property, "01/01/1970 01:02:03\n"),
        (Token.Text, "Traceback (most recent call last):\n"),
        (Token.Text, "  File "),
        (Token.Name.Builtin, '"filename"'),
        (Token.Text, ", line "),
        (Token.Literal.Number, "1546"),
        (Token.Text, ", in "),
        (Token.Name, "func"),
        (Token.Text, "\n"),
        (Token.Generic.Error, "RuntimeError"),
        (Token.Text, ": "),
        (Token.Name.Exception, "something happened"),
        (Token.Text, "\n"),
        (Token.Text, "\n"),
        (
            Token.Heading,
            "During handling of the above exception, another exception occurred:\n\n",
        ),
        (Token.Text, "Traceback (most recent call last):\n"),
        (Token.Text, "  File "),
        (Token.Name.Builtin, '"filename"'),
        (Token.Text, ", line "),
        (Token.Literal.Number, "1546"),
        (Token.Text, ", in "),
        (Token.Name, "func"),
        (Token.Text, "\n"),
        (Token.Generic.Error, "RuntimeError"),
        (Token.Text, ": "),
        (Token.Name.Exception, "something happened"),
        (Token.Text, "\n"),
        (Token.Text, "\n"),
        (
            Token.Heading,
            "The above exception was the direct cause of the following exception:\n\n",
        ),
        (Token.Text, "Traceback (most recent call last):\n"),
        (Token.Text, "  File "),
        (Token.Name.Builtin, '"filename"'),
        (Token.Text, ", line "),
        (Token.Literal.Number, "1546"),
        (Token.Text, ", in "),
        (Token.Name, "func"),
        (Token.Text, "\n"),
        (Token.Generic.Error, "RuntimeError"),
        (Token.Text, ": "),
        (Token.Name.Exception, "something happened"),
        (Token.Text, "\n"),
    ]
    assert tokens == expected_tokens
