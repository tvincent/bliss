# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import contextlib
import gevent

from prompt_toolkit.input import create_pipe_input
from prompt_toolkit.application import create_app_session
from prompt_toolkit.output import DummyOutput

from bliss.shell import getval as getval_module
from bliss.shell.getval import getval_yes_no, getval_name, getval_int_range
from bliss.shell.getval import getval_idx_list, getval_char_list
from bliss.common.utils import Undefined
from .conftest import asyncio_loop


def send_input(prompt, text):
    prompt.input.send_text(text)
    gevent.sleep(0.05)


@contextlib.contextmanager
def ptpython_session():
    # PipeInput object, for sending input to the CLI
    # (This is something that we can use in the prompt_toolkit event loop,
    # but still write date in manually.)
    with asyncio_loop():
        with create_pipe_input() as pipe_input:
            output = DummyOutput()
            with create_app_session(pipe_input, output) as ptpython_session:
                yield ptpython_session


@contextlib.contextmanager
def bliss_prompt_session():
    with ptpython_session():
        bliss_prompt_orig = getval_module._prompt_factory
        bp = bliss_prompt_orig()
        getval_module._prompt_factory = lambda: bp
        try:
            with gevent.Timeout(1, TimeoutError):
                yield bp
        except TimeoutError:
            if not bp.app.is_done:
                bp.app.exit()
            raise
        finally:
            getval_module._prompt_factory = bliss_prompt_orig


@pytest.mark.parametrize(
    "data",
    [
        ("y\n", True, Undefined),
        ("n\n", False, Undefined),
        ("yes\n", True, Undefined),
        ("YES\n", True, Undefined),
        ("NO\n", False, Undefined),
        ("yeS\n", True, Undefined),
        ("\x03", True, "yeah"),
        ("\x03", False, "yeah"),
    ],
)
def test_getval_yes_no(data):
    keyboard, expected, ki_default = data
    with bliss_prompt_session() as bps:
        send_input(bps, keyboard)
        result = getval_yes_no("do you want", default=False, ki_default=ki_default)
    if ki_default != Undefined:
        assert result == ki_default
    else:
        assert result is expected
    assert bps.default_buffer.validation_error is None


def test_getval_yes_no_invalid():
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            send_input(bps, "yp\n")
            getval_yes_no("do you want", default=False)
    assert "The input have to" in bps.default_buffer.validation_error.message


def test_getval_yes_no_keyboardinterrupt():
    with pytest.raises(KeyboardInterrupt):
        with bliss_prompt_session() as bps:
            send_input(bps, "\x03")
            getval_yes_no("do you want", default=False)


@pytest.mark.parametrize("data", [("titi\n", "titi"), ("tutu3\n", "tutu3")])
def test_getval_name(data):
    keyboard, expected = data
    with bliss_prompt_session() as bps:
        send_input(bps, keyboard + "\n")
        result = getval_name("Enter name", default=False)
    assert result == expected
    assert bps.default_buffer.validation_error is None


def test_getval_name_invalid():
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            send_input(bps, "1toto\n")
            getval_name("Enter name", default=False)
    assert bps.default_buffer.validation_error is not None


@pytest.mark.parametrize(
    "data",
    [
        (1, 10, "1\n", 1),
        (1, 10, "50\b\n", 5),
        (-1, 10, "-1\n", -1),
        (1, 10, "\n", 9),
    ],
)
def test_getval_int_range(data):
    minimum, maximum, keyboard, expected = data
    with bliss_prompt_session() as bps:
        send_input(bps, keyboard)
        result = getval_int_range("Give me", minimum, maximum, default=9)
    assert result == expected
    assert bps.default_buffer.validation_error is None


def test_getval_int_too_small():
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            send_input(bps, "1\n")
            getval_int_range("Give me", 2, 400, default=False)
    assert bps.default_buffer.validation_error is not None


def test_getval_int_too_big():
    with pytest.raises(TimeoutError):
        with bliss_prompt_session() as bps:
            send_input(bps, "401\n")
            getval_int_range("Give me", 2, 400, default=False)
    assert bps.default_buffer.validation_error is not None


def test_getval_idx_list():
    dspacing_list = ["111", "311", "642"]
    with bliss_prompt_session() as bps:
        send_input(bps, "2\n")
        result = getval_idx_list(dspacing_list, "enter value")
    assert result == (2, "311")
    assert bps.default_buffer.validation_error is None


def test_getval_char_list():
    actions_list = [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
    with bliss_prompt_session() as bps:
        send_input(bps, "\na\n")
        result = getval_char_list(actions_list, "enter value")
    assert result == ("a", "add a roi")
    assert bps.default_buffer.validation_error is None


def test_getval_char_list__default():
    actions_list = [("a", "add a roi"), ("r", "remove a roi"), ("m", "modify a roi")]
    with bliss_prompt_session() as bps:
        send_input(bps, "a\n")
        result = getval_char_list(actions_list, "enter value", default="a")
    assert result == ("a", "add a roi")
    assert bps.default_buffer.validation_error is None
