import asyncio
import contextlib
from prompt_toolkit.input.defaults import create_pipe_input
from prompt_toolkit.output import DummyOutput
from bliss.shell.cli.repl import BlissRepl

import gevent


@contextlib.contextmanager
def asyncio_loop():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        yield loop
    finally:
        loop.close()


@contextlib.contextmanager
def bliss_repl(locals_dict=None, confirm_exit=False):
    if locals_dict is None:
        locals_dict = {}
    with asyncio_loop():
        with create_pipe_input() as inp:
            try:
                br = BlissRepl(
                    input=inp,
                    output=DummyOutput(),
                    style="default",
                    get_globals=lambda: locals_dict,
                )
                br.confirm_exit = confirm_exit
                yield br
            finally:
                if br.app.is_running:
                    br.app.exit()
                BlissRepl.instance = None  # BlissRepl is a Singleton


def _feed_cli_with_input(
    text, check_line_ending=True, locals_dict=None, timeout=10, confirm_exit=False
):
    """
    Create a Prompt, feed it with the given user input and return the CLI
    object.

    Inspired by python-prompt-toolkit/tests/test_cli.py
    """
    # If the given text doesn't end with a newline, the interface won't finish.
    if check_line_ending:
        assert text.endswith("\r")

    with bliss_repl(locals_dict, confirm_exit) as br:
        with br.app.output.capture_stdout:
            br.app.input.send_text(text)

            with gevent.Timeout(timeout, TimeoutError):
                try:
                    result = br.app.run()
                except EOFError:
                    return

                return result, br.app, br
