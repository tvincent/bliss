# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
import gevent
import gevent.event

from bliss.common.logtools import userlogger
from bliss.common.shutter import BaseShutterState
from bliss.common.shutter import Shutter
from bliss.common import event
from bliss.testutils.rpc_controller import controller_in_another_process


class SimulationShutter(Shutter):
    def __init__(self, name, config):
        super().__init__(name, config)
        self.__internal_state = BaseShutterState.CLOSED

    def _state(self):
        return self.__internal_state

    def _set_mode(self, value):
        pass

    def _open(self):
        self.__internal_state = BaseShutterState.OPEN

    def _close(self):
        self.__internal_state = BaseShutterState.CLOSED


@pytest.fixture
def simulation_shutter(beacon):
    return beacon.get("sim_shutter_test")


@pytest.fixture
def simulation_shutter_in_another_process(beacon, beacon_host_port):
    beacon_host, beacon_port = beacon_host_port
    with controller_in_another_process(
        beacon_host, beacon_port, "sim_shutter_test"
    ) as c:
        yield c


def test_simulation_shutter(
    beacon, simulation_shutter, simulation_shutter_in_another_process
):
    external_simulation_shutter = simulation_shutter_in_another_process
    assert external_simulation_shutter.state_string == simulation_shutter.state_string
    assert simulation_shutter.state_string == "Closed"

    simulation_shutter.open()

    assert simulation_shutter.state_string == "Open"

    with gevent.Timeout(3):
        while external_simulation_shutter.is_closed:
            # let some time for channel propagation to the external process
            gevent.sleep(0.02)
    assert external_simulation_shutter.state_string == simulation_shutter.state_string

    cb_called_event = gevent.event.AsyncResult()

    def state_changed_event(state):
        cb_called_event.set(state)

    event.connect(simulation_shutter, "state", state_changed_event)

    # close shutter from the second process, and ensure we got called via state change event
    try:
        external_simulation_shutter.close()
        with gevent.Timeout(3):
            cb_called_event.wait()
        assert cb_called_event.value == BaseShutterState.CLOSED
    finally:
        event.disconnect(simulation_shutter, "state", state_changed_event)


def test_shutter_ext_control(beacon, log_context):
    userlogger.enable()

    # Get config.
    shutter = beacon.get("simulation_shutter_ext_control")

    shutter.external_control.set("CLOSED")
    shutter.mode = shutter.EXTERNAL
    assert shutter.state == BaseShutterState.CLOSED
    shutter.external_control.set("OPEN")
    assert shutter.state == BaseShutterState.OPEN
    shutter.close()
    assert shutter.external_control.get() == "CLOSED"


def test_shutter__config_deprecation(default_session, log_context, capsys):
    userlogger.enable()

    # Make a "deprecated" version of the config.
    shutter_cfg = default_session.config.get_config("simulation_shutter_ext_control")
    shutter_cfg["external-control"] = shutter_cfg["external_control"]
    del shutter_cfg["external_control"]

    _ = SimulationShutter(shutter_cfg["name"], shutter_cfg)

    # Test that "deprecated" version of the config produces a warning message.
    assert "please use 'external_control'" in capsys.readouterr().err
