import numpy
from bliss.controllers.demo.sample_stage_diode import SampleStageDiode


def test_image_minmax_coords():
    img = numpy.arange(200)
    img.shape = 20, 10
    config = {
        "data": img,
        "min_corner": [100, 100],
        "max_corner": [300, 200],
        "axis1": None,
        "axis2": None,
    }
    diode = SampleStageDiode("foo", config)
    assert diode._axis_to_image(100, 100) == (0, 0)
    assert diode._axis_to_image(200, 150) == (10, 5)
    assert diode._axis_to_image(300, 200) == (20, 10)


def test_image_scale_coords():
    img = numpy.arange(200)
    img.shape = 20, 10
    config = {
        "data": img,
        "scale": 0.1,
        "axis1": None,
        "axis2": None,
    }
    diode = SampleStageDiode("foo", config)
    assert diode._axis_to_image(1, 1) == (20, 15)
    assert diode._axis_to_image(0, 0) == (10, 5)
    assert diode._axis_to_image(-2, -2) == (-10, -15)
