# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest


@pytest.fixture
def temp_tloop(beacon):
    obj = beacon.get("sample_regulation")
    yield obj
    obj.close()
    obj.controller.close()


@pytest.fixture
def temp_soft_tloop(beacon):
    obj = beacon.get("soft_regul")
    yield obj
    obj.close()
    obj.input.device.close()


@pytest.fixture
def temp_soft_tloop_2(beacon):
    obj = beacon.get("soft_regul2")
    yield obj
    obj.close()
    obj.input.device.close()


@pytest.fixture
def temp_tango_loop(beacon, regulation_tango_server):
    obj = beacon.get("loopds")
    yield obj
    obj.close()
