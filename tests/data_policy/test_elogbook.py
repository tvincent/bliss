import itertools

from bliss.shell.standard import elog_print
from bliss.common import logtools


def test_elog_print(session, esrf_data_policy, icat_mock_client):
    elog_print("message1")
    icat_mock_client.return_value.send_message.assert_called_once_with(
        "message1",
        beamline_only=None,
        msg_type="comment",
        tags=[session.name],
    )


def test_electronic_logbook(session, esrf_data_policy, icat_mock_client):
    lst = [
        ("info", "info"),
        ("warning", "warning"),
        ("error", "error"),
        ("debug", "debug"),
        ("critical", "critical"),
        ("command", "command"),
        ("comment", "comment"),
        ("print", "comment"),
    ]
    beamline_only = [True, False, None]
    try:
        for param in itertools.product(lst, beamline_only, beamline_only):
            (method_name, msg_type), beamline_only, default_beamline_only = param
            logtools.elogbook.beamline_only = default_beamline_only
            msg = repr(method_name + " message")
            method = getattr(logtools.elogbook, method_name)
            if beamline_only is None:
                method(msg)
                effective_beamline_only = logtools.elogbook.beamline_only
            else:
                method(msg, beamline_only=beamline_only)
                effective_beamline_only = beamline_only
            try:
                icat_mock_client.return_value.send_message.assert_called_once_with(
                    msg,
                    beamline_only=effective_beamline_only,
                    msg_type=msg_type,
                    tags=[session.name],
                )
                icat_mock_client.reset_mock()
            except AssertionError as e:
                raise AssertionError(str(param)) from e
    finally:
        logtools.elogbook.beamline_only = None
