import pytest
import os
from bliss.shell.standard import get_source_code


@pytest.fixture
def prdef_session(beacon):
    session = beacon.get("test_prdef_session")
    session.setup()
    yield session
    session.close()


@pytest.fixture
def prdef_user_script(tmpdir):
    filename = tmpdir / "user_test_script.py"
    with open(filename, "w") as f:
        f.write("# Still comment should not be visible\n")
        f.write("\n\n")
        f.write("def dummy_user():\n")
        f.write("    pass\n")
        f.write("\n\n")
        f.write("def prdef_example_user():\n")
        f.write("    return 100\n")
    yield filename


def modify_script(filename, replacemap):
    with open(filename, "r") as f:
        content = list(f)
    content = [replacemap.get(line, line) for line in content]
    with open(filename, "w") as f:
        f.writelines(content)


def modify_return_value(filename, old, new):
    replacemap = {f"    return {old}\n": f"    return {new}\n"}
    modify_script(filename, replacemap)


def assert_source_code(func, expected_return_value, script_path, line_number):
    expected_header = f"'{func.__name__}' is defined in:\n{script_path}:{line_number}\n"
    expected_content = [
        f"def {func.__name__}():\n",
        f"    return {expected_return_value}\n",
    ]
    header, content = get_source_code(func)
    assert header == expected_header
    assert content == expected_content


def test_prdef_session_setup_script(prdef_session, beacon_directory):
    script_path = os.path.join(beacon_directory, "sessions", "test_prdef_session.py")

    func = prdef_session.env_dict["prdef_example_setup"]
    assert func() == 1
    assert_source_code(func, 1, "beacon://" + script_path, 10)

    modify_return_value(script_path, 1, 2)
    func = prdef_session.env_dict["prdef_example_setup"]
    assert func() == 1
    assert_source_code(func, 1, "beacon://" + script_path, 10)

    prdef_session.resetup()
    func = prdef_session.env_dict["prdef_example_setup"]
    assert func() == 2
    assert_source_code(func, 2, "beacon://" + script_path, 10)


def test_prdef_session_script(prdef_session, beacon_directory):
    script_path = os.path.join(
        beacon_directory, "sessions", "scripts", "prdef_script.py"
    )
    load_script = prdef_session.env_dict["load_script"]

    func = prdef_session.env_dict["prdef_example_script"]
    assert func() == 10
    assert_source_code(func, 10, "beacon://" + script_path, 8)

    modify_return_value(script_path, 10, 20)
    func = prdef_session.env_dict["prdef_example_script"]
    assert func() == 10
    assert_source_code(func, 10, "beacon://" + script_path, 8)

    load_script("prdef_script")
    func = prdef_session.env_dict["prdef_example_script"]
    assert func() == 20
    assert_source_code(func, 20, "beacon://" + script_path, 8)


def test_prdef_user_script(prdef_session, prdef_user_script):
    script_path = str(prdef_user_script)
    user_script_homedir = prdef_session.env_dict["user_script_homedir"]
    user_script_load = prdef_session.env_dict["user_script_load"]
    user_script_homedir(prdef_user_script.dirname)
    user_script_load(prdef_user_script.basename)

    func = prdef_session.env_dict["user"].prdef_example_user
    assert func() == 100
    assert_source_code(func, 100, script_path, 8)

    modify_return_value(script_path, 100, 200)
    func = prdef_session.env_dict["user"].prdef_example_user
    assert func() == 100
    assert_source_code(func, 100, script_path, 8)

    user_script_load(prdef_user_script.basename)
    func = prdef_session.env_dict["user"].prdef_example_user
    assert func() == 200
    assert_source_code(func, 200, script_path, 8)
