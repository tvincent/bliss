# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
import pytest
import gevent

from bliss.common import scans
from bliss.scanning.scan import ScanState
from blissdata.data.nodes import channel


@pytest.fixture
def limit_channel_max_len():
    save = channel.CHANNEL_MAX_LEN
    channel.CHANNEL_MAX_LEN = 20
    yield
    channel.CHANNEL_MAX_LEN = save


def test_get_data(session, limit_channel_max_len):
    diode = session.config.get("diode")

    # ==== CASE 0: data is still in Redis -> OK
    scan = scans.loopscan(20, 0.001, diode, save=False)
    data = scan.get_data()
    for k, v in data.items():
        assert len(v) == 20

    # ==== CASE 1a: data channel was trimmed in Redis, but saved on file -> OK
    scan = scans.loopscan(50, 0.001, diode, save=True)
    data = scan.get_data()
    for k, v in data.items():
        assert len(v) == 50

    # ==== CASE 1b: data channel was trimmed in Redis, not saved neither -> FAIL
    scan = scans.loopscan(50, 0.001, diode, save=False)
    with pytest.raises(
        RuntimeError,
        match="Unsaved scan have expired on Redis, please use saved scans for later access.",
    ):
        data = scan.get_data()

    # ==== CASE 2a: data expired in Redis, but saved on file -> OK
    scan = scans.loopscan(20, 0.001, diode, save=True)

    # delete diode data stream manually to mimic expiration
    for node in scan.nodes.values():
        if node.name == "simulation_diode_sampling_controller:diode":
            node.connection.delete(node.db_name + "_data")
            break
    else:
        raise Exception("Failed to force diode data expiration on Redis")

    data = scan.get_data()
    for k, v in data.items():
        assert len(v) == 20

    # ==== CASE 2b: data expired in Redis, not saved neither-> FAIL
    scan = scans.loopscan(20, 0.001, diode, save=False)

    # delete diode data stream manually to mimic expiration
    for node in scan.nodes.values():
        if node.name == "simulation_diode_sampling_controller:diode":
            node.connection.delete(node.db_name + "_data")
            break
    else:
        raise Exception("Failed to force diode data expiration on Redis")

    with pytest.raises(
        RuntimeError,
        match="Unsaved scan have expired on Redis, please use saved scans for later access.",
    ):
        data = scan.get_data()

    # ==== CASE 3: scan is running, get_data return partial data
    scan = scans.loopscan(20, 0.1, diode, save=False, run=False)
    glt = gevent.spawn(scan.run)
    gevent.sleep(1)  # let some time for the scan to start
    data = scan.get_data()
    assert scan.state is not ScanState.DONE
    for k, v in data.items():
        assert len(v) < 20
    glt.get()
