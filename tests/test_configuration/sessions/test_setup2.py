from bliss.shell.standard import *  # noqa: F403
from bliss import is_bliss_shell

# Do not remove this print (used in tests)
print("TEST_SESSION2 INITIALIZED")

load_script("script1")  # noqa: F405

if is_bliss_shell():
    protect("toto")  # noqa: F405

var1 = 1
var2 = 2

if is_bliss_shell():
    protect(["var1", "var2"])  # noqa: F405
