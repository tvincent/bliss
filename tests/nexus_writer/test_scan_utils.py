# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from glob import glob
from nexus_writer_service.utils import scan_utils
from nexus_writer_service.io import nexus
from bliss.common import scans
from tests.nexus_writer.helpers import nxw_test_utils


WRITER_OPTIONS = ("nexus", "nexus_separate_scan_files", "hdf5", "null")
WRITER_OPTIONS = ("nexus",)


@pytest.mark.parametrize("data_writer", WRITER_OPTIONS)
@pytest.mark.parametrize("save", (True, False))
def test_scan_utils(data_writer, save, nexus_writer_config):
    _test_scan_utils(data_writer=data_writer, save=save, **nexus_writer_config)


@pytest.mark.parametrize("data_writer", WRITER_OPTIONS)
@pytest.mark.parametrize("save", (True, False))
def test_scan_utils_nopolicy(data_writer, save, nexus_writer_config_nopolicy):
    _test_scan_utils(data_writer=data_writer, save=save, **nexus_writer_config_nopolicy)


@pytest.mark.parametrize("data_writer", WRITER_OPTIONS)
@pytest.mark.parametrize("save", (True, False))
def test_scan_utils_base(data_writer, save, nexus_writer_config):
    _test_scan_utils(data_writer=data_writer, save=save, **nexus_writer_config)


@pytest.mark.parametrize("data_writer", WRITER_OPTIONS)
@pytest.mark.parametrize("save", (True, False))
def test_scan_utils_base_nopolicy(data_writer, save, nexus_writer_config_nopolicy):
    _test_scan_utils(data_writer=data_writer, save=save, **nexus_writer_config_nopolicy)


@nxw_test_utils.writer_stdout_on_exception
def _test_scan_utils(
    session=None,
    tmpdir=None,
    config=True,
    policy=True,
    data_writer=None,
    save=None,
    writer=None,
    **_,
):
    separate_scan_files = data_writer == "nexus_separate_scan_files"
    if separate_scan_files:
        data_writer = "nexus"
    session.scan_saving.writer = data_writer

    if separate_scan_files:
        writer_object = session.scan_saving.writer_object
        writer_object.separate_scan_files = separate_scan_files

    scan = scans.sct(0.1, session.env_dict["diode3"], save=save)

    # Expected file names based in the policy alone (ignore save/writer settings)
    if policy:
        proposal_session_name = session.scan_saving.proposal_session_name
        scan_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            "RAW_DATA",
            "sample",
            "sample_0001",
            "scan0001",
            "bliss_master.h5",
        )
        dataset_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            proposal_session_name,
            "RAW_DATA",
            "sample",
            "sample_0001",
            "sample_0001.h5",
        )
        collection_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            proposal_session_name,
            "RAW_DATA",
            "sample",
            "testproposal_sample.h5",
        )
        proposal_filename = tmpdir.join(
            session.name,
            "fs1",
            "id00",
            "tmp",
            "testproposal",
            "id00",
            proposal_session_name,
            "RAW_DATA",
            "testproposal_id00.h5",
        )
        master_filenames = {
            "dataset_collection": collection_filename,
            "proposal": proposal_filename,
        }
        filenames = {"dataset": dataset_filename}
    else:
        scan_filename = tmpdir.join(session.name, "scan0001", "bliss_master.h5")
        dataset_filename = tmpdir.join(session.name, "default_filename.h5")
        master_filenames = dict()
        filenames = {"dataset": dataset_filename}
    if separate_scan_files:
        master_filenames["dataset"] = dataset_filename
        filenames["scan"] = scan_filename
    else:
        scan_filename = dataset_filename
    filenames.update(master_filenames)

    # Check file existence based on policy/save/writer settings
    saves_files = save and data_writer != "null"
    if saves_files:
        nxw_test_utils.wait_scan_data_exists([scan], writer=writer)
    saves_extra_masters = saves_files and data_writer == "nexus" and config
    assert scan_filename.check(file=1) == saves_files, scan_filename
    assert dataset_filename.check(file=1) == saves_files, dataset_filename
    for name, f in master_filenames.items():
        assert f.check(file=1) == saves_extra_masters, f
    for name, f in filenames.items():
        expected = saves_files and (name in ("dataset", "scan") or saves_extra_masters)
        assert f.check(file=1) == expected, f

    # Remove unexpected files based on writer settings
    saves_files = data_writer != "null"
    saves_extra_masters = saves_files and data_writer == "nexus" and config
    if not saves_files:
        scan_filename = ""
        dataset_filename = ""
        master_filenames = dict()
        filenames = dict()
    elif not saves_extra_masters:
        for name in ("dataset_collection", "proposal"):
            for lst in (filenames, master_filenames):
                lst.pop(name, None)

    # Check file names from session (save settings are irrelevant)
    session_filenames = dict(filenames)
    session_filenames.pop("scan", None)
    assert scan_utils.session_filename() == dataset_filename
    assert scan_utils.session_master_filenames(config=config) == master_filenames
    assert scan_utils.session_filenames(config=config) == session_filenames

    # Check scan uri
    if data_writer == "nexus":
        scan_uri = str(scan_filename) + "::/1.1"
    elif data_writer == "hdf5":
        scan_uri = str(scan_filename) + "::/1_ct"
    else:
        scan_uri = str(scan_filename)
    if scan_uri:
        assert nexus.exists(scan_uri) == (save and data_writer != "null")
    else:
        assert data_writer == "null"

    # Remove unexpected files based on save/writer settings
    saves_files = save and data_writer != "null"
    saves_extra_masters = saves_files and data_writer == "nexus" and config
    if not saves_files:
        scan_filename = ""
        scan_uri = ""
        master_filenames = dict()
        filenames = dict()
    elif not saves_extra_masters:
        for name in ("dataset_collection", "proposal"):
            for lst in (filenames, master_filenames):
                lst.pop(name, None)

    # Check file names from scan object
    assert scan_utils.scan_filename(scan) == scan_filename
    assert scan_utils.scan_filename(scan.node) == scan_filename
    assert scan_utils.scan_uri(scan) == scan_uri
    assert scan_utils.scan_uri(scan.node) == scan_uri
    assert scan_utils.scan_master_filenames(scan, config=config) == master_filenames
    assert scan_utils.scan_filenames(scan, config=config) == filenames
    assert (
        scan_utils.scan_master_filenames(scan.node, config=config) == master_filenames
    )
    assert scan_utils.scan_filenames(scan.node, config=config) == filenames

    # Check file names from directory
    found = set(glob(str(tmpdir.join("**", "*.h5")), recursive=True))
    expected = set(filter(None, filenames.values()))
    assert found == expected
