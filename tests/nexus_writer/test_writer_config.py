# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import pytest
from nexus_writer_service.utils import config_utils
from nexus_writer_service.utils import scan_utils
from nexus_writer_service.subscribers import scan_writer_publish


@pytest.mark.parametrize("separate_scan_files", (True, False))
def test_config_withoutpolicy(separate_scan_files, nexus_writer_config_nopolicy):
    session = nexus_writer_config_nopolicy["session"]
    tmpdir = nexus_writer_config_nopolicy["tmpdir"]
    writer_object = session.scan_saving.writer_object
    writer_object.separate_scan_files = separate_scan_files
    validate_writer_config(scan_writer_publish.writer_config())
    assert config_utils.beamline() == "id00"
    assert config_utils.institute() == "ESRF"
    assert config_utils.instrument() == "esrf-id00a"
    assert scan_writer_publish.default_technique() == "none"
    assert scan_writer_publish.current_technique() == "none"
    filenames = scan_utils.session_filenames(config=True)
    expected_filenames = {"dataset": tmpdir.join(session.name, "default_filename.h5")}
    assert filenames == expected_filenames


@pytest.mark.parametrize("separate_scan_files", (True, False))
def test_config_withpolicy(separate_scan_files, nexus_writer_config):
    session = nexus_writer_config["session"]
    tmpdir = nexus_writer_config["tmpdir"]
    writer_object = session.scan_saving.writer_object
    writer_object.separate_scan_files = separate_scan_files
    validate_writer_config(scan_writer_publish.writer_config())
    assert config_utils.beamline() == "id00"
    assert config_utils.institute() == "ESRF"
    assert config_utils.instrument() == "esrf-id00a"
    assert scan_writer_publish.default_technique() == "none"
    assert scan_writer_publish.current_technique() == "xrfxrd"
    filenames = scan_utils.session_filenames(config=True)
    proposal_session_name = session.scan_saving.proposal_session_name
    expected_filenames = dict()
    expected_filenames["dataset"] = tmpdir.join(
        session.name,
        "fs1",
        "id00",
        "tmp",
        "testproposal",
        "id00",
        proposal_session_name,
        "RAW_DATA",
        "sample",
        "sample_0001",
        "sample_0001.h5",
    )
    expected_filenames["dataset_collection"] = tmpdir.join(
        session.name,
        "fs1",
        "id00",
        "tmp",
        "testproposal",
        "id00",
        proposal_session_name,
        "RAW_DATA",
        "sample",
        "testproposal_sample.h5",
    )
    expected_filenames["proposal"] = tmpdir.join(
        session.name,
        "fs1",
        "id00",
        "tmp",
        "testproposal",
        "id00",
        proposal_session_name,
        "RAW_DATA",
        "testproposal_id00.h5",
    )
    assert filenames == expected_filenames


def validate_writer_config(cfg):
    assert {"name", "technique"} == set(cfg.keys())
    assert {"default", "techniques", "applications", "plots"} == set(
        cfg["technique"].keys()
    )
