import os
import time
import numpy
import h5py
import tango

from Lima import Simulator as LimaSimuMod
import Lima.Server.camera.Simulator as TangoSimuMod


class LimaDiffSimulator(LimaSimuMod.Camera):
    def __init__(self):
        super().__init__()
        self._counter = 0
        self._images = None
        self._mask = None
        self._data_filename = None
        self._loaded = False

    def set_data_filename(self, data_filename):
        self._data_filename = os.path.expandvars(data_filename)

    def init_img(self):
        """Initialize resources.

        It is not done during object construction to speed up
        the device initialization.
        """
        if self._loaded:
            return
        with h5py.File(self._data_filename, mode="r") as f:
            self._images = f["scan1/instrument/data"][...]
            if self._images.ndim == 2:
                self._images = self._images[None, ...]
            self._counter = 0
            self._mask = f["scan1/mask"][...]
        self._loaded = True

    def get_img(self):
        img = self._images[self._counter]
        self._counter += 1
        if self._counter >= self._images.shape[0]:
            self._counter = 0
        return img

    def fillData(self, data):
        """
        Callback function
        Called for every frame in a different C++ thread.
        """
        self.init_img()
        image = self.get_img()

        # Inverse ramp from 100% to 20% restarting back every 'duration' seconds
        duration = 20
        intensity = int(time.time()) % duration
        intensity = 0.1 + 0.9 * (duration - intensity) / duration
        image = numpy.random.poisson(image.astype(float)) * intensity

        # Put back the dead pixels and the gaps between modules
        image[self._mask != 0] = 65535

        # Clamp the image to the result data
        width = min(data.buffer.shape[0], image.shape[0])
        height = min(data.buffer.shape[1], image.shape[1])
        data.buffer[...] = 0
        data.buffer[0:width, 0:height] = image[0:width, 0:height]


class DiffSimulator(TangoSimuMod.Simulator):
    def init_device(self):
        TangoSimuMod.Simulator.init_device(self)
        self._SimuCamera.set_data_filename(self.data_filename)


class DiffSimulatorClass(TangoSimuMod.SimulatorClass):
    # Device Properties
    device_property_list = {
        "data_filename": [tango.DevString, "filename containing the data", []]
    }
    device_property_list.update(TangoSimuMod.SimulatorClass.device_property_list)


def get_control(**kwargs):
    return TangoSimuMod.get_control(
        **kwargs, _Simulator=DiffSimulator, _Camera=LimaDiffSimulator
    )


def get_tango_specific_class_n_device():
    return DiffSimulatorClass, DiffSimulator
